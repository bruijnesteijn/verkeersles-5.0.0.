//
//  VLQuestionsForExamViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLQuestionsForExamViewController : UIViewController <UITraitEnvironment, UIContentContainer> {
    NSArray *dataSource;
    
    NSString *typeOfExam;
    int numberOfExam;
    
    int currentQuestionNumber;
    
    IBOutlet UIStackView *questionStackView;
    IBOutlet UIImageView *questionImage;
    IBOutlet UILabel *questionText;
    BOOL questionHidden;
    
    CGFloat totalWidth;
    CGFloat totalHeight;
    CGFloat percentageOfTotalWidth;
    CGFloat calculatedWidth;
    IBOutlet NSLayoutConstraint *widthQuestionTextWidthConstraint;
    IBOutlet NSLayoutConstraint *widthQuestionTextMarginConstraint;
   
    IBOutlet UIStackView *answersStackView;

    IBOutlet UILabel *questionAnswerLabelA;
    IBOutlet UILabel *questionAnswerLabelB;
    IBOutlet UILabel *questionAnswerLabelC;
    
    IBOutlet UIImageView *questionAnswerImageViewA;
    IBOutlet UIImageView *questionAnswerImageViewB;
    IBOutlet UIImageView *questionAnswerImageViewC;
    
    IBOutlet UIStackView *questionAnswerStackViewA;
    IBOutlet UIStackView *questionAnswerStackViewB;
    IBOutlet UIStackView *questionAnswerStackViewC;
    
    IBOutlet UIButton *rightNavigationBarButton;

    NSString *givenAnswerForQuestion;
    NSString *correctAnswerForQuestion;
    
    IBOutlet UIProgressView *progressbar;
    float progressbarStep;
    
    NSTimer *timeToAnswerTheQuestionTimer;
    BOOL answeringQuestionForAnswer;
    
    NSTimer *timeForShowingTheAnswerTimer;
    BOOL viewingAnswerForQuestion;
    BOOL viewingErrorForQuestion;
}

@property (nonatomic, retain) NSArray *dataSource;

@property (nonatomic, retain) NSString *typeOfExam;
@property int numberOfExam;

@property (nonatomic, retain) IBOutlet UIStackView *questionStackView;
@property (nonatomic, retain) IBOutlet UIImageView *questionImage;
@property (nonatomic, retain) IBOutlet UILabel *questionText;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *widthQuestionTextWidthConstraint;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *widthQuestionTextMarginConstraint;

@property (nonatomic, retain) IBOutlet UIStackView *answersStackView;

@property (nonatomic, retain) IBOutlet UILabel *questionAnswerLabelA;
@property (nonatomic, retain) IBOutlet UILabel *questionAnswerLabelB;
@property (nonatomic, retain) IBOutlet UILabel *questionAnswerLabelC;

@property (nonatomic, retain) IBOutlet UIImageView *questionAnswerImageViewA;
@property (nonatomic, retain) IBOutlet UIImageView *questionAnswerImageViewB;
@property (nonatomic, retain) IBOutlet UIImageView *questionAnswerImageViewC;

@property (nonatomic, retain) IBOutlet UIStackView *questionAnswerStackViewA;
@property (nonatomic, retain) IBOutlet UIStackView *questionAnswerStackViewB;
@property (nonatomic, retain) IBOutlet UIStackView *questionAnswerStackViewC;

@property (nonatomic, retain) IBOutlet UIButton *rightNavigationBarButton;

@property (nonatomic, retain) IBOutlet UIProgressView *progressbar;

- (IBAction)nextPressed:(id)sender;
- (IBAction)pressedQuestionStackView:(id)sender;

@end
