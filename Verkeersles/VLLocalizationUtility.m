//
//  VLLocalizationUtility.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLLocalizationUtility.h"

@implementation VLLocalizationUtility

static NSBundle *bundle = nil;  

+ (void)initialize {  
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];  
	NSArray *languages = [defaults objectForKey:@"AppleLanguages"]; 
	NSString *selectedLanguage = [languages objectAtIndex:0]; 
	
	[self setLanguage:selectedLanguage];  
}

+ (void)setLanguage:(NSString *)newLanguage { 
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];  
	[defaults setObject:newLanguage forKey:@"language"];

	NSString *path = [[NSBundle mainBundle] pathForResource:newLanguage ofType:@"lproj"];  
	bundle = [NSBundle bundleWithPath:path];
}

+ (NSString *)getLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];  

    return [defaults objectForKey:@"language"];
}

+ (NSString *)localizedString:(NSString *)key alter:(NSString *)alternate {  
	return [bundle localizedStringForKey:key value:alternate table:nil]; 
} 

@end 