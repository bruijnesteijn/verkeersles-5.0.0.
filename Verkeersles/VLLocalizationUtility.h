//
//  VLLocalizationUtility.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef NSLocalizedString
#undef NSLocalizedString
#endif

#define NSLocalizedString(s, c) ([VLLocalizationUtility localizedString:s alter:c])

@interface VLLocalizationUtility : NSObject {

}

+ (void)initialize;
+ (void)setLanguage:(NSString *)newLanguage;
+ (NSString *)getLanguage;
+ (NSString *)localizedString:(NSString *)key alter:(NSString *)alternate;

@end
