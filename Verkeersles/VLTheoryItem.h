//
//  VLTheoryItem.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijnon 7/9/11.
//  Copyright (c) 2011 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VLTheoryCategory;

@interface VLTheoryItem : NSManagedObject {
}

@property (nonatomic, retain) NSString * theoryItemAnswer;
@property (nonatomic, retain) NSString * theoryItemAntwoord;
@property (nonatomic, retain) NSString * theoryItemQuestion;
@property (nonatomic, retain) NSString * theoryItemVraag;
@property (nonatomic, retain) NSString * theoryItemID;

@property (nonatomic, retain) VLTheoryCategory * belongsToVLTheoryCategory;

@end
