//
//  VLExam.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 15/12/15.
//  Copyright © 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLExam : NSObject {
    NSString *examID;
    NSString *examNumberOfQuestions;
    NSString *examNumberOfQuestionsAnswered;
    NSString *examNumberOfQuestionsAnsweredCorrect;
}

@property (nonatomic, retain) NSString *examID;
@property (nonatomic, retain) NSString *examNumberOfQuestions;
@property (nonatomic, retain) NSString *examNumberOfQuestionsAnswered;
@property (nonatomic, retain) NSString *examNumberOfQuestionsAnsweredCorrect;

- (instancetype)init;

@end
