//
//  VLListOfArticlesForSectionTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfArticlesForSectionTableViewController : UITableViewController {
    NSDictionary *dataSource;
    int section;
}

@property (nonatomic, retain) NSDictionary *dataSource;
@property (nonatomic) int section;

@end
