//
//  VLListOfTheoryCategoriesTableViewCell.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/03/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfTheoryCategoriesTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *image;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;

@end
