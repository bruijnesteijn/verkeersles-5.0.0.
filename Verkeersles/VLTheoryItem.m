//
//  VLTheoryItem.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijnon 7/9/11.
//  Copyright (c) 2011 2010 PiecesOfMyMind Software. All rights reserved.
//

#import "VLTheoryItem.h"
#import "VLTheoryCategory.h"

@implementation VLTheoryItem

@dynamic theoryItemAnswer;
@dynamic theoryItemAntwoord;
@dynamic theoryItemQuestion;
@dynamic theoryItemVraag;
@dynamic theoryItemID;

@dynamic belongsToVLTheoryCategory;

@end
