//
//  VLQuestionsForExamViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLQuestionsForExamViewController.h"
#import "VLAppDelegate.h"
#import "VLLocalizationUtility.h"
#import "VLConstants.h"

@interface VLQuestionsForExamViewController(Private)

- (void)initializeDataSource;

- (void)controller;
- (void)completed;

- (void) hideQuestionTextStackView;
- (void) showQuestionTextStackView;

- (void)initializeTimer;
- (void)setTimerToZero;
- (void)setTimerToFull;
- (void)increaseTimer;
- (void)decreaseTimer;
- (void)invalidateQuestionTimer;
- (void)invalidateAnswerTimer;

- (void)initializeQuestion;
- (void)initializeAnswer;
- (void)initializeError;
- (void)initializeCurrentQuestionNumber;

- (void)pressedAnswerA;
- (void)pressedAnswerB;
- (void)pressedAnswerC;

- (void)saveAnswerForQuestion;

@end

@implementation VLQuestionsForExamViewController

@synthesize dataSource, typeOfExam, numberOfExam,
    questionStackView, answersStackView,
    questionImage, questionText, widthQuestionTextWidthConstraint, widthQuestionTextMarginConstraint,
    questionAnswerLabelA, questionAnswerLabelB, questionAnswerLabelC,
    questionAnswerImageViewA, questionAnswerImageViewB, questionAnswerImageViewC,
    questionAnswerStackViewA, questionAnswerStackViewB, questionAnswerStackViewC,
    rightNavigationBarButton,
    progressbar;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapRecognizerQuestion = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressedQuestionStackView:)];
    tapRecognizerQuestion.numberOfTapsRequired = 1;
    [self.questionStackView addGestureRecognizer:tapRecognizerQuestion];

    UITapGestureRecognizer *tapRecognizerA = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressedAnswerA)];
    tapRecognizerA.numberOfTapsRequired = 1;
    [self.questionAnswerStackViewA addGestureRecognizer:tapRecognizerA];
    
    UITapGestureRecognizer *tapRecognizerB = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressedAnswerB)];
    tapRecognizerB.numberOfTapsRequired = 1;
    [self.questionAnswerStackViewB addGestureRecognizer:tapRecognizerB];

    UITapGestureRecognizer *tapRecognizerC = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressedAnswerC)];
    tapRecognizerC.numberOfTapsRequired = 1;
    [self.questionAnswerStackViewC addGestureRecognizer:tapRecognizerC];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];
    
    [self.navigationItem setTitle:NSLocalizedString(typeOfExam, typeOfExam)];
    
    [[self navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"QuestionTitle", @"QuestionTitle"), currentQuestionNumber + 1]];
    
    currentQuestionNumber = -1;
    
    totalWidth = self.view.frame.size.width;

    self.questionText.adjustsFontSizeToFitWidth = YES;
    self.widthQuestionTextWidthConstraint.constant = 0.0;
    self.widthQuestionTextMarginConstraint.constant = 10.0;
    questionHidden = TRUE;

    [self controller];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self pressedQuestionStackView:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if(timeToAnswerTheQuestionTimer != nil) {
        [self invalidateQuestionTimer];
    }
    if(timeForShowingTheAnswerTimer != nil) {
        [self invalidateAnswerTimer];
    }
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"QuestionTitle", @"QuestionTitle"), currentQuestionNumber + 1]];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];

    totalWidth = size.width;

    if(!questionHidden) {
        [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.8 initialSpringVelocity:2.0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.widthQuestionTextWidthConstraint.constant = 0.0;
            self.widthQuestionTextMarginConstraint.constant = 0.0;
            self.questionImage.alpha = 1.0;
            self.questionStackView.spacing = 0.0;
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            questionHidden = TRUE;
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)pressedQuestionStackView:(id)sender {
    if(questionHidden) {
        [self showQuestionTextStackView ];
    }
    else {
        [self hideQuestionTextStackView ];
    }
}

- (IBAction)nextPressed:(id)sender {
    if (answeringQuestionForAnswer) {
        [progressbar setProgress:1.0];
        [self increaseTimer];
    }
    else if (viewingAnswerForQuestion){
        [progressbar setProgress:0.0];
        [self decreaseTimer];
    }
    else if (viewingErrorForQuestion){
        [progressbar setProgress:0.0];
        [self decreaseTimer];
    }
}

@end

@implementation VLQuestionsForExamViewController(Private)

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *file = [NSString stringWithFormat:NSLocalizedString(@"VLExam_datasource", @"VLExam_datasource"), numberOfExam];
    NSString *plistPath = [path stringByAppendingPathComponent:file];
    
    self.dataSource = [NSArray arrayWithContentsOfFile:plistPath];
    
    self.questionText.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    self.questionText.textColor = QUESTION_COLOR;
    self.questionAnswerLabelA.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    self.questionAnswerLabelB.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    self.questionAnswerLabelC.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
}

- (void)controller {
    if (dataSource) {
        [self initializeTimer];

        [self initializeCurrentQuestionNumber];
        
        if(currentQuestionNumber < 30) {
            if([typeOfExam isEqualToString:@"Practise"]) {
                [self setTimerToZero];
            
                [self initializeQuestion];
            }
            else if([typeOfExam isEqualToString:@"Exam"]) {
                [self setTimerToZero];
            
                [self initializeQuestion];
            }
            else if([typeOfExam isEqualToString:@"Errors"]) {
                [self setTimerToFull];
            
                [self initializeError];
            }
        }
        else {
            [self completed];
        }
    }
    else {
        //
    }
}

- (void)completed {
}

- (void)initializeTimer {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *numberOfSecondsToShowQuestion = [defaults stringForKey:@"numberOfSecondsToShowQuestion"];
    NSString *numberOfSecondsToAnswerTheQuestion = [defaults stringForKey:@"numberOfSecondsToAnswerTheQuestion"];
    NSString *numberOfSecondsToShowAnswer = [defaults stringForKey:@"numberOfSecondsToShowAnswer"];
    NSString *numberOfSecondsToShowError = [defaults stringForKey:@"numberOfSecondsToShowError"];
    
    if (numberOfSecondsToShowQuestion == nil) {
        numberOfSecondsToShowQuestion = @"8";
    }
    if (numberOfSecondsToAnswerTheQuestion == nil) {
        numberOfSecondsToAnswerTheQuestion = @"12";
    }
    if (numberOfSecondsToShowAnswer == nil) {
        numberOfSecondsToShowAnswer = @"8";
    }
    if (numberOfSecondsToShowError == nil) {
        numberOfSecondsToShowError = @"8";
    }
    
    if([typeOfExam isEqualToString:@"Practise"]) {
        progressbarStep = 1/([numberOfSecondsToAnswerTheQuestion floatValue] + [numberOfSecondsToShowQuestion floatValue]);
    }
    else if([typeOfExam isEqualToString:@"Exam"]) {
        progressbarStep = 1/([numberOfSecondsToAnswerTheQuestion floatValue] + [numberOfSecondsToShowQuestion floatValue]);
    }
    else if([typeOfExam isEqualToString:@"Errors"]) {
        progressbarStep = 1/([numberOfSecondsToShowAnswer floatValue] + [numberOfSecondsToShowError floatValue]);
    }
}

- (void)setTimerToZero {
    [progressbar setProgress:0];
}

- (void)setTimerToFull {
    [progressbar setProgress:1];
}

- (void)increaseTimer {
    if ([progressbar progress] < 1) {
        float newProgressbarStatus = [progressbar progress] + progressbarStep;
        [progressbar setProgress:newProgressbarStatus];
    }
    else {
        if([typeOfExam isEqualToString:@"Practise"]) {
            [self invalidateQuestionTimer];
            
            [self setTimerToFull];
            
            [self initializeAnswer];
        }
        else if([typeOfExam isEqualToString:@"Exam"]) {
            [self invalidateQuestionTimer];
            
            [self saveAnswerForQuestion];
            
            [self initializeCurrentQuestionNumber];
            
            if(currentQuestionNumber < 30) {
                [self setTimerToZero];
            
                [self initializeQuestion];
            }
            else {
                [self completed];
            }
        }
        else if([typeOfExam isEqualToString:@"Errors"]) {
        }
    }
}

- (void)decreaseTimer {
    if ([progressbar progress] > 0) {
        float newProgressbarStatus = [progressbar progress] - progressbarStep;
        [progressbar setProgress:newProgressbarStatus];
    }
    else {
        if([typeOfExam isEqualToString:@"Practise"]) {
            [self invalidateAnswerTimer];
            
            [self initializeCurrentQuestionNumber];
            
            if(currentQuestionNumber < 30) {
                [self setTimerToZero];
            
                [self initializeQuestion];
            }
            else {
                [self completed];
            }
        }
        else if([typeOfExam isEqualToString:@"Exam"]) {
        }
        else if([typeOfExam isEqualToString:@"Errors"]) {
            [self invalidateAnswerTimer];
            
            [self initializeCurrentQuestionNumber];
            
            if(currentQuestionNumber < 30) {
                [self setTimerToFull];
                
                [self initializeError];
            }
            else {
                [self completed];
            }
        }
    }
}

- (void)invalidateQuestionTimer {
    [timeToAnswerTheQuestionTimer invalidate];
    timeToAnswerTheQuestionTimer = nil;
}

- (void)invalidateAnswerTimer {
    [timeForShowingTheAnswerTimer invalidate];
    timeForShowingTheAnswerTimer = nil;
}

- (void) initializeCurrentQuestionNumber {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [NSString stringWithFormat:@"VLExamAnswers_%d_%d.plist", APP_EXAM_ANSWERS_FILENAME_VERSION, numberOfExam];
    NSString *documentsPath = [documentsDirectory stringByAppendingPathComponent:file];
    NSDictionary *previousAnswers = [NSDictionary dictionaryWithContentsOfFile:documentsPath];
    
    if([typeOfExam isEqualToString:@"Practise"]) {
        currentQuestionNumber++;
    }
    else if([typeOfExam isEqualToString:@"Exam"]) {
        if (currentQuestionNumber < 0) {
            currentQuestionNumber = [[previousAnswers valueForKey:@"totaalAantalBeantwoord"] intValue];
        }
        else {
            currentQuestionNumber++;
        }
    }
    else if([typeOfExam isEqualToString:@"Errors"]) {
        currentQuestionNumber++;
        
        if(currentQuestionNumber < 30) {
            do {
                if ([[[[previousAnswers valueForKey:@"antwoorden"] objectAtIndex:currentQuestionNumber] valueForKey:@"antwoordWasGoed"] isEqualToString:@"NO"]) {
                    break;
                }
            
                currentQuestionNumber++;
            }  while ((currentQuestionNumber < [[previousAnswers valueForKey:@"totaalAantal"] intValue]));
        }
    }
    
    if (currentQuestionNumber >= 30) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [[self navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"QuestionTitle", @"QuestionTitle"), currentQuestionNumber + 1]];
    }
}

- (void)initializeQuestion {
    self.questionImage.alpha = 0.0;
    
    answeringQuestionForAnswer = TRUE;
    viewingAnswerForQuestion = FALSE;
    viewingErrorForQuestion = FALSE;

    NSDictionary *question = [dataSource objectAtIndex:currentQuestionNumber];
    correctAnswerForQuestion = [question valueForKey:@"antwoord"];

    NSString *imageName = [NSString stringWithFormat:@"vl_%0*d_%0*d", 2, numberOfExam, 2, currentQuestionNumber + 1];
    [self.questionImage setImage:[UIImage imageNamed:imageName]];

    [self.questionText setText:[question objectForKey:@"vraag"]];
    [self.questionText setTextColor:QUESTION_COLOR];
    
    [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewA setAlpha:1.0];
    [self.questionAnswerLabelA setText:@""];
    [self.questionAnswerLabelA setAlpha:1.0];
    [self.questionAnswerStackViewA setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewB setAlpha:1.0];
    [self.questionAnswerLabelB setText:@""];
    [self.questionAnswerLabelB setAlpha:1.0];
    [self.questionAnswerStackViewB setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewC setAlpha:1.0];
    [self.questionAnswerLabelC setText:@""];
    [self.questionAnswerLabelC setAlpha:1.0];
    [self.questionAnswerStackViewC setUserInteractionEnabled:TRUE];

    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:0] isEqualToString:@""]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_A"]];
        [self.questionAnswerLabelA setText:[[question objectForKey:@"antwoorden"] objectAtIndex:0]];
        [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:1] isEqualToString:@""]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_B"]];
        [self.questionAnswerLabelB setText:[[question objectForKey:@"antwoorden"] objectAtIndex:1]];
        [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:2] isEqualToString:@""]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_C"]];
        [self.questionAnswerLabelC setText:[[question objectForKey:@"antwoorden"] objectAtIndex:2]];
        [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
    }
    
    [self showQuestionTextStackView ];
    
    timeToAnswerTheQuestionTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(increaseTimer) userInfo:nil repeats:YES];
}

- (void)initializeAnswer {
    answeringQuestionForAnswer = FALSE;
    viewingAnswerForQuestion = TRUE;
    viewingErrorForQuestion = FALSE;
    
    NSDictionary *question = [dataSource objectAtIndex:currentQuestionNumber];
    correctAnswerForQuestion = [question valueForKey:@"antwoord"];
    
    NSString *imageName = [NSString stringWithFormat:@"vl_%0*d_%0*d", 2, numberOfExam, 2, currentQuestionNumber + 1];
    [self.questionImage setImage:[UIImage imageNamed:imageName]];
    
    [self.questionText setText:[question objectForKey:@"uitleg"]];
    [self.questionText setTextColor:ANSWER_COLOR];

    [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewA setAlpha:1.0];
    [self.questionAnswerLabelA setText:@""];
    [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelA setAlpha:1.0];
    [self.questionAnswerStackViewA setUserInteractionEnabled:FALSE];
    
    [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewB setAlpha:1.0];
    [self.questionAnswerLabelB setText:@""];
    [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelB setAlpha:1.0];
    [self.questionAnswerStackViewB setUserInteractionEnabled:FALSE];
    
    [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewC setAlpha:1.0];
    [self.questionAnswerLabelC setText:@""];
    [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelC setAlpha:1.0];
    [self.questionAnswerStackViewC setUserInteractionEnabled:FALSE];

    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:0] isEqualToString:@""]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_A"]];
        [self.questionAnswerLabelA setText:[[question objectForKey:@"antwoorden"] objectAtIndex:0]];
        [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
        if ([givenAnswerForQuestion isEqualToString:@"A"]) {
            [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"incorrect"]];
            [self.questionAnswerLabelA setTextColor:FALSE_COLOR];
        }
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:1] isEqualToString:@""]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_B"]];
        [self.questionAnswerLabelB setText:[[question objectForKey:@"antwoorden"] objectAtIndex:1]];
        [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
        if ([givenAnswerForQuestion isEqualToString:@"B"]) {
            [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"incorrect"]];
            [self.questionAnswerLabelB setTextColor:FALSE_COLOR];
        }
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:2] isEqualToString:@""]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_C"]];
        [self.questionAnswerLabelC setText:[[question objectForKey:@"antwoorden"] objectAtIndex:2]];
        [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
        if ([givenAnswerForQuestion isEqualToString:@"C"]) {
            [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"incorrect"]];
            [self.questionAnswerLabelC setTextColor:FALSE_COLOR];
        }
    }
    
    if ([correctAnswerForQuestion isEqualToString:@"A"]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"correct"]];
        [self.questionAnswerLabelA setTextColor:CORRECT_COLOR];
    }
    else if ([correctAnswerForQuestion isEqualToString:@"B"]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"correct"]];
        [self.questionAnswerLabelB setTextColor:CORRECT_COLOR];
    }
    else if ([correctAnswerForQuestion isEqualToString:@"C"]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"correct"]];
        [self.questionAnswerLabelC setTextColor:CORRECT_COLOR];
    }
    
    [self showQuestionTextStackView ];
    
    timeForShowingTheAnswerTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(decreaseTimer) userInfo:nil repeats:YES];
}

- (void)initializeError {
    answeringQuestionForAnswer = FALSE;
    viewingAnswerForQuestion = FALSE;
    viewingErrorForQuestion = TRUE;
    
    NSDictionary *question = [dataSource objectAtIndex:currentQuestionNumber];
    correctAnswerForQuestion = [question valueForKey:@"antwoord"];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [NSString stringWithFormat:@"VLExamAnswers_%d_%d.plist", APP_EXAM_ANSWERS_FILENAME_VERSION, numberOfExam];
    NSString *documentsPath = [documentsDirectory stringByAppendingPathComponent:file];
    NSDictionary *answers = [NSDictionary dictionaryWithContentsOfFile:documentsPath];

    NSArray *answer = [[answers valueForKey:@"antwoorden"] objectAtIndex:currentQuestionNumber];
    correctAnswerForQuestion = [answer valueForKey:@"juisteAntwoord"];
    givenAnswerForQuestion = [answer valueForKey:@"gegevenAntwoord"];
    
    NSString *imageName = [NSString stringWithFormat:@"vl_%0*d_%0*d", 2, numberOfExam, 2, currentQuestionNumber + 1];
    [self.questionImage setImage:[UIImage imageNamed:imageName]];
    
    [self.questionText setText:[question objectForKey:@"uitleg"]];
    [self.questionText setTextColor:ANSWER_COLOR];

    [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewA setAlpha:1.0];
    [self.questionAnswerLabelA setText:@""];
    [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelA setAlpha:1.0];
    [self.questionAnswerStackViewA setUserInteractionEnabled:FALSE];
    
    [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewB setAlpha:1.0];
    [self.questionAnswerLabelB setText:@""];
    [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelB setAlpha:1.0];
    [self.questionAnswerStackViewB setUserInteractionEnabled:FALSE];
    
    [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewC setAlpha:1.0];
    [self.questionAnswerLabelC setText:@""];
    [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelC setAlpha:1.0];
    [self.questionAnswerStackViewC setUserInteractionEnabled:FALSE];
    
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:0] isEqualToString:@""]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_A"]];
        [self.questionAnswerLabelA setText:[[question objectForKey:@"antwoorden"] objectAtIndex:0]];
        [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
        if ([givenAnswerForQuestion isEqualToString:@"A"]) {
            [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"incorrect"]];
            [self.questionAnswerLabelA setTextColor:FALSE_COLOR];
        }
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:1] isEqualToString:@""]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_B"]];
        [self.questionAnswerLabelB setText:[[question objectForKey:@"antwoorden"] objectAtIndex:1]];
        [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
        if ([givenAnswerForQuestion isEqualToString:@"B"]) {
            [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"incorrect"]];
            [self.questionAnswerLabelB setTextColor:FALSE_COLOR];
        }
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:2] isEqualToString:@""]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_C"]];
        [self.questionAnswerLabelC setText:[[question objectForKey:@"antwoorden"] objectAtIndex:2]];
        [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
        if ([givenAnswerForQuestion isEqualToString:@"C"]) {
            [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"incorrect"]];
            [self.questionAnswerLabelC setTextColor:FALSE_COLOR];
        }
    }
    
    if ([correctAnswerForQuestion isEqualToString:@"A"]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"correct"]];
        [self.questionAnswerLabelA setTextColor:CORRECT_COLOR];
    }
    else if ([correctAnswerForQuestion isEqualToString:@"B"]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"correct"]];
        [self.questionAnswerLabelB setTextColor:CORRECT_COLOR];
    }
    else if ([correctAnswerForQuestion isEqualToString:@"C"]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"correct"]];
        [self.questionAnswerLabelC setTextColor:CORRECT_COLOR];
    }
    
    [self showQuestionTextStackView ];
    
    timeForShowingTheAnswerTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(decreaseTimer) userInfo:nil repeats:YES];
}

- (void)pressedAnswerA {
    NSDictionary *question = [dataSource objectAtIndex:currentQuestionNumber];
    
    [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewA setAlpha:1.0];
    [self.questionAnswerLabelA setText:@""];
    [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelA setAlpha:1.0];
    [self.questionAnswerStackViewA setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewB setAlpha:1.0];
    [self.questionAnswerLabelB setText:@""];
    [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelB setAlpha:1.0];
    [self.questionAnswerStackViewB setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewC setAlpha:1.0];
    [self.questionAnswerLabelC setText:@""];
    [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelC setAlpha:1.0];
    [self.questionAnswerStackViewC setUserInteractionEnabled:TRUE];
    
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:0] isEqualToString:@""]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_A_selected"]];
        [self.questionAnswerLabelA setText:[[question objectForKey:@"antwoorden"] objectAtIndex:0]];
        [self.questionAnswerLabelA setTextColor:SELECTED_COLOR];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:1] isEqualToString:@""]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_B"]];
        [self.questionAnswerImageViewB setAlpha:1.0];
        [self.questionAnswerLabelB setText:[[question objectForKey:@"antwoorden"] objectAtIndex:1]];
        [self.questionAnswerLabelB setAlpha:0.6];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:2] isEqualToString:@""]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_C"]];
        [self.questionAnswerImageViewC setAlpha:1.0];
        [self.questionAnswerLabelC setText:[[question objectForKey:@"antwoorden"] objectAtIndex:2]];
        [self.questionAnswerLabelC setAlpha:0.6];
    }
    
    givenAnswerForQuestion = @"A";
}

- (void)pressedAnswerB {
    NSDictionary *question = [dataSource objectAtIndex:currentQuestionNumber];
    
    [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewA setAlpha:1.0];
    [self.questionAnswerLabelA setText:@""];
    [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelA setAlpha:1.0];
    [self.questionAnswerStackViewA setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewB setAlpha:1.0];
    [self.questionAnswerLabelB setText:@""];
    [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelB setAlpha:1.0];
    [self.questionAnswerStackViewB setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewC setAlpha:1.0];
    [self.questionAnswerLabelC setText:@""];
    [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelC setAlpha:1.0];
    [self.questionAnswerStackViewC setUserInteractionEnabled:TRUE];
    
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:0] isEqualToString:@""]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_A"]];
        [self.questionAnswerImageViewA setAlpha:1.0];
        [self.questionAnswerLabelA setText:[[question objectForKey:@"antwoorden"] objectAtIndex:0]];
        [self.questionAnswerLabelA setAlpha:0.6];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:1] isEqualToString:@""]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_B_selected"]];
        [self.questionAnswerLabelB setText:[[question objectForKey:@"antwoorden"] objectAtIndex:1]];
        [self.questionAnswerLabelB setTextColor:SELECTED_COLOR];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:2] isEqualToString:@""]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_C"]];
        [self.questionAnswerImageViewC setAlpha:1.0];
        [self.questionAnswerLabelC setText:[[question objectForKey:@"antwoorden"] objectAtIndex:2]];
        [self.questionAnswerLabelC setAlpha:0.6];
    }
    
    givenAnswerForQuestion = @"B";
}

- (void)pressedAnswerC {
    NSDictionary *question = [dataSource objectAtIndex:currentQuestionNumber];
    
    [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewA setAlpha:1.0];
    [self.questionAnswerLabelA setText:@""];
    [self.questionAnswerLabelA setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelA setAlpha:1.0];
    [self.questionAnswerStackViewA setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewB setAlpha:1.0];
    [self.questionAnswerLabelB setText:@""];
    [self.questionAnswerLabelB setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelB setAlpha:1.0];
    [self.questionAnswerStackViewB setUserInteractionEnabled:TRUE];
    
    [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_none"]];
    [self.questionAnswerImageViewC setAlpha:1.0];
    [self.questionAnswerLabelC setText:@""];
    [self.questionAnswerLabelC setTextColor:DEFAULT_COLOR];
    [self.questionAnswerLabelC setAlpha:1.0];
    [self.questionAnswerStackViewC setUserInteractionEnabled:TRUE];
    
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:0] isEqualToString:@""]) {
        [self.questionAnswerImageViewA setImage:[UIImage imageNamed:@"Char_A"]];
        [self.questionAnswerImageViewA setAlpha:0.6];
        [self.questionAnswerLabelA setText:[[question objectForKey:@"antwoorden"] objectAtIndex:0]];
        [self.questionAnswerLabelA setAlpha:0.6];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:1] isEqualToString:@""]) {
        [self.questionAnswerImageViewB setImage:[UIImage imageNamed:@"Char_B"]];
        [self.questionAnswerImageViewB setAlpha:0.6];
        [self.questionAnswerLabelB setText:[[question objectForKey:@"antwoorden"] objectAtIndex:1]];
        [self.questionAnswerLabelB setAlpha:0.6];
    }
    if (![[[question objectForKey:@"antwoorden"] objectAtIndex:2] isEqualToString:@""]) {
        [self.questionAnswerImageViewC setImage:[UIImage imageNamed:@"Char_C_selected"]];
        [self.questionAnswerLabelC setText:[[question objectForKey:@"antwoorden"] objectAtIndex:2]];
        [self.questionAnswerLabelC setTextColor:SELECTED_COLOR];
    }
    
    givenAnswerForQuestion = @"C";
}

- (void)saveAnswerForQuestion {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [NSString stringWithFormat:@"VLExamAnswers_%d_%d.plist", APP_EXAM_ANSWERS_FILENAME_VERSION, numberOfExam];
    NSString *documentsPath = [documentsDirectory stringByAppendingPathComponent:file];
    NSMutableDictionary *results = [NSMutableDictionary dictionaryWithContentsOfFile:documentsPath];
    
    NSDictionary *result = [[results valueForKey:@"antwoorden"] objectAtIndex:currentQuestionNumber];
    
    int count = [[results valueForKey:@"totaalAantalBeantwoord"] intValue] + 1;
    [results setValue:[NSString stringWithFormat:@"%d", count] forKey:@"totaalAantalBeantwoord"];
    
    [result setValue:correctAnswerForQuestion forKey:@"juisteAntwoord"];
    [result setValue:givenAnswerForQuestion forKey:@"gegevenAntwoord"];
    
    if ([givenAnswerForQuestion isEqualToString:correctAnswerForQuestion]) {
        int ok = [[results valueForKey:@"totaalAantalGoed"] intValue] + 1;
        [results setValue:[NSString stringWithFormat:@"%d", ok] forKey:@"totaalAantalGoed"];
        
        [result setValue:@"YES" forKey:@"antwoordWasGoed"];
    }
    else {
        [result setValue:@"NO" forKey:@"antwoordWasGoed"];
    }
    
    [results writeToFile:documentsPath atomically:YES];
}

- (void) hideQuestionTextStackView {
    [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.8 initialSpringVelocity:2.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.widthQuestionTextWidthConstraint.constant = 0.0;
        self.widthQuestionTextMarginConstraint.constant = 0.0;
        self.questionImage.alpha = 1.0;
        self.questionStackView.spacing = 0.0;
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        questionHidden = TRUE;
    }];
}

- (void) showQuestionTextStackView {
    [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.8 initialSpringVelocity:2.0 options:UIViewAnimationOptionCurveLinear animations:^{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([[defaults stringForKey:@"showImagesWithText"] isEqualToString:@"NO"]) {
            self.questionImage.alpha = 0.0;
            self.questionStackView.spacing = 10.0;
            percentageOfTotalWidth = 0.9;
            calculatedWidth = percentageOfTotalWidth * totalWidth;
        }
        else {
            self.questionImage.alpha = 0.2;
            self.questionStackView.spacing = -100.0;
            percentageOfTotalWidth = 0.8;
            calculatedWidth = percentageOfTotalWidth * totalWidth;
        }

        self.widthQuestionTextWidthConstraint.constant = calculatedWidth;// MIN(calculatedWidth, 400);
        self.widthQuestionTextMarginConstraint.constant = 10.0;
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        questionHidden = FALSE;
    }];
}

@end