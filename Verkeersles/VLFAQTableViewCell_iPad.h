//
//  VLFAQTableViewCell_iPad.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 2/22/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLFAQTableViewCell_iPad : UITableViewCell {
	IBOutlet UITextView *faqAnswerTextView;
}

@property (nonatomic, retain) IBOutlet UITextView *faqAnswerTextView;

@end
