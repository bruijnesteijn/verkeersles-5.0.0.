//
//  VLListOfTrafficSignCategoriesTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/19/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import "VLListOfTrafficSignCategoriesTableViewController.h"
#import "VLListOfTrafficSignCategoriesTableViewCell.h"
#import "VLListOfTrafficSignsTableViewController.h"
#import "VLTrafficSignExamViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfTrafficSignCategoriesTableViewController(Private)

- (void)initializeDataSource;
- (void)refreshTableView;

@end

@implementation VLListOfTrafficSignCategoriesTableViewController

@synthesize dataSource;

#pragma mark -
#pragma mark View lifecycle

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficSignsTitle", @"TrafficSignsTitle")];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficSignsTitle", @"TrafficSignsTitle")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Table view data source

- (VLListOfTrafficSignCategoriesTableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLCategoryCell";

    VLListOfTrafficSignCategoriesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
  
    [cell setNeedsLayout];
    [cell layoutIfNeeded];

    return cell;
}

- (void)configureCell:(VLListOfTrafficSignCategoriesTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [dataSource objectAtIndex:indexPath.row];

    [self setTitleForCell:cell labelText:[NSString stringWithFormat:@"%@", [dict objectForKey:@"categorie"]]];
    [self setSubtitleForCell:cell labelText:[NSString stringWithFormat:@"%@", [dict objectForKey:@"beschrijving"]]];
    [self setImageForCell:cell imageName:[NSString stringWithFormat:@"%@", [[dict objectForKey:@"id"] lowercaseString]]];
}

- (void)setTitleForCell:(VLListOfTrafficSignCategoriesTableViewCell *)cell labelText:(NSString *)text {
    [cell.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [cell.titleLabel setNumberOfLines:0];
    [cell.titleLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.titleLabel setText:text];
}

- (void)setSubtitleForCell:(VLListOfTrafficSignCategoriesTableViewCell *)cell labelText:(NSString *)text {
    [cell.subtitleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.subtitleLabel setNumberOfLines:0];
    [cell.subtitleLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.subtitleLabel setText:text];
}

- (void)setImageForCell:(VLListOfTrafficSignCategoriesTableViewCell *)cell imageName:(NSString *)text {
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@1", text]];
    [cell.image setImage:img];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [self cellAtIndexPath:indexPath];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataSource count];
}

#pragma mark -
#pragma mark Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showListOfTrafficSignsForCategory"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        VLListOfTrafficSignsTableViewController *gallery = segue.destinationViewController;
        gallery.category = (int) indexPath.row;
            
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
    if ([segue.identifier isEqualToString:@"showExamForTrafficSigns"]) {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
    if ([segue.identifier isEqualToString:@"showSelectionCriteriaForTrafficSigns"]) {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
}

@end

@implementation VLListOfTrafficSignCategoriesTableViewController(Private)

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = nil;
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"onlyRVVSigns"] isEqualToString:@"YES"]) {
        plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSignRVV_datasource", @"VLTrafficSignRVV_datasource")];
    }
    else {
        plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSign_datasource", @"VLTrafficSign_datasource")];
    }
    self.dataSource = [NSArray arrayWithContentsOfFile:plistPath];
    
    [self.tableView reloadData];
}

- (void)refreshTableView {
    [self.tableView setHidden:NO];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.tableView setHidden:YES];
    } completion:^(BOOL finished){
    
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:[dataSource count]-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath: indexPath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    
        double delay = 0.3;
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
        dispatch_after(time, dispatch_get_main_queue(), ^{
            NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView scrollToRowAtIndexPath: path atScrollPosition: UITableViewScrollPositionTop animated: YES];
        
            double delay = 0.3;
            dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
            dispatch_after(time, dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.3f animations:^{
                    [self.tableView setHidden:NO];
                } completion:nil];
            });
        });
    }];
}

@end



