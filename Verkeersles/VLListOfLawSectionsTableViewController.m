//
//  VLListOfLawSectionsTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLListOfLawSectionsTableViewController.h"
#import "VLListOfLawSectionsTableViewCell.h"
#import "VLListOfArticlesForSectionTableViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfLawSectionsTableViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLListOfLawSectionsTableViewController

@synthesize dataSource;

#pragma mark -
#pragma mark View lifecycle

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Traffic Law", @"Traffic Law")];
    
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];

    [[self navigationItem] setTitle:NSLocalizedString(@"Traffic Law", @"Traffic Law")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Table view data source

- (VLListOfLawSectionsTableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLLawSectionCell";
    
    VLListOfLawSectionsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(VLListOfLawSectionsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [dataSource objectAtIndex:indexPath.row];
    
    [self setTitleForCell:cell labelText:[NSString stringWithFormat:@"%@", [dict objectForKey:@"categorie"]]];
    [self setSubtitleForCell:cell labelText:[NSString stringWithFormat:@"%@", [dict objectForKey:@"beschrijving"]]];
    [self setImageForCell:cell imageName:[NSString stringWithFormat:@"%@", [dict objectForKey:@"image"]]];
}

- (void)setTitleForCell:(VLListOfLawSectionsTableViewCell *)cell labelText:(NSString *)text {
    [cell.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [cell.titleLabel setNumberOfLines:0];
    [cell.titleLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.titleLabel setText:text];
}

- (void)setSubtitleForCell:(VLListOfLawSectionsTableViewCell *)cell labelText:(NSString *)text {
    [cell.subtitleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.subtitleLabel setNumberOfLines:0];
    [cell.subtitleLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.subtitleLabel setText:text];
}

- (void)setImageForCell:(VLListOfLawSectionsTableViewCell *)cell imageName:(NSString *)text {
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@", text]];
    [cell.image setImage:img];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellAtIndexPath:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataSource count];
}

#pragma mark -
#pragma mark Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showListOfArticlesForSection"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        VLListOfArticlesForSectionTableViewController *section = segue.destinationViewController;
        section.section = (int) indexPath.row;
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
}

@end

@implementation VLListOfLawSectionsTableViewController(Private)

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficLaws_datasource", @"VLTrafficLaws_datasource")];
    
    self.dataSource = [[NSDictionary dictionaryWithContentsOfFile:plistPath] objectForKey:@"regels"];
    
    [self.tableView reloadData];
}

@end



