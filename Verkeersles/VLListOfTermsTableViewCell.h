//
//  VLListOfTermsTableViewCell.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 15/10/14.
//  Copyright (c) 2014 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfTermsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;

@end
