//
//  VLListOfTrafficSignsViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLListOfTrafficSignsPageContentViewController.h"

@interface VLListOfTrafficSignsViewController : UIViewController <UIPageViewControllerDelegate, UIPageViewControllerDataSource> {
    NSDictionary *dataSource;
    int category;
    
    NSMutableArray *selectedTrafficSigns;
    
    UIPageViewController *pageViewController;
}

@property (nonatomic, retain) NSDictionary *dataSource;
@property (nonatomic) int category;

@property (strong, nonatomic) NSMutableArray *selectedTrafficSigns;

@property (strong, nonatomic) UIPageViewController *pageViewController;

- (VLListOfTrafficSignsPageContentViewController *)viewControllerAtIndex:(NSUInteger)index;

@end
