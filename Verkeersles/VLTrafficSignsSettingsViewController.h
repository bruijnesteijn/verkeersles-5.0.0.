//
//  VLTrafficSignsSettingsViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLTrafficSignsSettingsViewController : UITableViewController <UITextFieldDelegate> {
    IBOutlet UILabel *lblOnlyRVVSigns;
	IBOutlet UISwitch *onlyRVVSigns;
    IBOutlet UILabel *lblShowSignsInRandomOrder;
    IBOutlet UISwitch *showSignsInRandomOrder;
    
    IBOutlet UILabel *lblNumberOfSecondsToShowQuestionForSign;
	IBOutlet UITextField *numberOfSecondsToShowQuestionForSign;
    IBOutlet UILabel *lblNumberOfSecondsToShowAnswerForSign;
	IBOutlet UITextField *numberOfSecondsToShowAnswerForSign;
    
    IBOutlet UILabel *lblSeconds1;
    IBOutlet UILabel *lblSeconds2;
}

@property (nonatomic, retain) IBOutlet UILabel *lblOnlyRVVSigns;
@property (nonatomic, retain) IBOutlet UILabel *lblShowSignsInRandomOrder;

@property (nonatomic, retain) IBOutlet UILabel *lblNumberOfSecondsToShowQuestionForSign;
@property (nonatomic, retain) IBOutlet UILabel *lblNumberOfSecondsToShowAnswerForSign;

@property (nonatomic, retain) IBOutlet UISwitch *onlyRVVSigns;
@property (nonatomic, retain) IBOutlet UISwitch *showSignsInRandomOrder;

@property (nonatomic, retain) IBOutlet UITextField *numberOfSecondsToShowQuestionForSign;
@property (nonatomic, retain) IBOutlet UITextField *numberOfSecondsToShowAnswerForSign;

@property (nonatomic, retain) IBOutlet UILabel *lblSeconds1;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds2;

@end
