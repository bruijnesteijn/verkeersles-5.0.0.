//
//  VLListOfItemsForCategoryTableViewHeaderCell.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 2/22/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfItemsForCategoryTableViewHeaderCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *questionLabel;
@property (nonatomic, weak) IBOutlet UIButton *header;

@end
