//
//  VLListOfTermsTableViewController_iPhone.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLListOfTermsTableViewController.h"
#import "VLListOfTermsTableViewCell.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfTermsTableViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLListOfTermsTableViewController

@synthesize dataSource, keys;

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self initializeDataSource];

    [[self navigationItem] setTitle:NSLocalizedString(@"Begrippen", @"Begrippen")];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    dataSource = nil;
    keys = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Begrippen", @"Begrippen")];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [keys objectAtIndex:section];
    NSArray *termSection = [dataSource objectForKey:key];
    
    return [termSection count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *key = [keys objectAtIndex:section];
    
    return key;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return keys;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return TABLEVIEW_SECTION_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width - 20.0, TABLEVIEW_SECTION_HEIGHT)];
    customView.backgroundColor = TABLEVIEW_SECTION_COLOR;
    customView.opaque = YES;
    customView.alpha = 0.6;
    
    UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    customLabel.backgroundColor = [UIColor clearColor];
    customLabel.opaque = NO;
    customLabel.textColor = TABLEVIEW_SECTION_TEXTCOLOR;
    customLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    customLabel.frame = CGRectMake(10, 0, tableView.bounds.size.width - 20.0, TABLEVIEW_SECTION_HEIGHT);
    
    NSString *key = [keys objectAtIndex:section];
    customLabel.text = [NSString stringWithString:key];
    
    [customView addSubview:customLabel];
    
    return customView;
}

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLTerminologyCell";
    
    VLListOfTermsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(VLListOfTermsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [keys objectAtIndex:[indexPath section]];
    NSArray *termSection = [dataSource objectForKey:key];
    
    NSDictionary *dict = [termSection objectAtIndex:indexPath.row];
    
    [self setTitleForCell:cell labelText:[NSString stringWithFormat:@"%@", [dict objectForKey:@"begrip"]]];
    [self setSubTitleForCell:cell labelText:[NSString stringWithFormat:@"%@", [dict objectForKey:@"uitleg"]]];
}

- (void)setTitleForCell:(VLListOfTermsTableViewCell *)cell labelText:(NSString *)text {
    [cell.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [cell.titleLabel setNumberOfLines:0];
    [cell.titleLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.titleLabel setText:text];
}

- (void)setSubTitleForCell:(VLListOfTermsTableViewCell *)cell labelText:(NSString *)text {
    [cell.subtitleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.subtitleLabel setNumberOfLines:0];
    [cell.subtitleLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.subtitleLabel setText:text];
}

@end

@implementation VLListOfTermsTableViewController(Private)

- (void)initializeDataSource
{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficTerminology_datasource", @"VLTrafficTerminology_datasource")];
    self.dataSource = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    self.keys = [[self.dataSource allKeys] sortedArrayUsingSelector:@selector(compare:)];

    [self.tableView reloadData];
}

@end
