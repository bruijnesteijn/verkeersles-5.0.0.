//
//  VLFAQTableViewCell_iPad.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 2/22/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import "VLFAQTableViewCell_iPad.h"

@interface VLFAQTableViewCell_iPad(Private)

- (void)releaseIBOutlets;

@end

@implementation VLFAQTableViewCell_iPad

@synthesize faqAnswerTextView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)dealloc {
	[self releaseIBOutlets];
	
    [super dealloc];
}

@end

@implementation VLFAQTableViewCell_iPad(Private)

- (void)releaseIBOutlets {
	self.faqAnswerTextView = nil;
}

@end
