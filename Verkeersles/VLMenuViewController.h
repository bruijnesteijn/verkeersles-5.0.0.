//
//  VLMenuViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLMenuViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *menuOption1;
@property (strong, nonatomic) IBOutlet UIButton *menuOption2;
@property (strong, nonatomic) IBOutlet UIButton *menuOption3;
@property (strong, nonatomic) IBOutlet UIButton *menuOption4;
@property (strong, nonatomic) IBOutlet UIButton *menuOption5;
@property (strong, nonatomic) IBOutlet UIButton *menuOption6;

@end
