//
//  VLTrafficSignExamViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import "VLTrafficSignExamViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLTrafficSignExamViewController(Private)

- (void)initializeDataSource;
- (void)updateProgressbar;
- (void)selectCategoryAndSign;
- (void)stopProgressbar;
- (void)startProgressbar;
- (void)releaseIBOutlets;
- (IBAction)viewTapped:(UITapGestureRecognizer*) recognizer;

@end

@implementation VLTrafficSignExamViewController

@synthesize dataSource, signImage, signCategory, signDescription, signProgressbar;

#pragma mark -
#pragma mark Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.signImage setAlpha:0.0f];
    [self.signCategory setAlpha:0.0f];
    [self.signDescription setAlpha:0.0f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self initializeDataSource];

    selectedCategory = -1;
    selectedSign = -1;
	[self selectCategoryAndSign];
    
    showingQuestion = FALSE;
    [self stopProgressbar];

    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficSignExamQuestionTitle", @"TrafficSignExamQuestionTitle")];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if(signProgressbarTimer != nil) {
        [signProgressbarTimer invalidate];
        signProgressbarTimer = nil;
    }
	
	dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [self setTitle:NSLocalizedString(@"TrafficSignExamQuestionTitle", @"TrafficSignExamQuestionTitle")];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    [self releaseIBOutlets];
}

#pragma mark -
#pragma mark Finalization

- (void)dealloc {
	[self releaseIBOutlets];
}

@end

@implementation VLTrafficSignExamViewController(Private)

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = nil;
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"onlyRVVSigns"] isEqualToString:@"YES"]) {
        plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSignRVV_datasource", @"VLTrafficSignRVV_datasource")];
    }
    else {
        plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSign_datasource", @"VLTrafficSign_datasource")];
    }
    
    self.dataSource = [NSArray arrayWithContentsOfFile:plistPath];
}

- (void)updateProgressbar {
    if(showingQuestion) {
        if (signProgressbar.progress < 1.0) {
            float nextStep = [signProgressbar progress] + signProgressbarStep;
            [signProgressbar setProgress:nextStep];
        }
        else {
            [self stopProgressbar];
        }
    }
    else {
        if (signProgressbar.progress > 0.0) {
            float nextStep = [signProgressbar progress] - signProgressbarStep;
            [signProgressbar setProgress:nextStep];
        }
        else {
            [self selectCategoryAndSign];
            
            [self stopProgressbar];
        }
    }
}

- (void)selectCategoryAndSign {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
	if ([[defaults stringForKey:@"showSignsInRandomOrder"] isEqualToString:@"YES"]) {
		selectedCategory = arc4random() % [dataSource count];
		selectedSign = arc4random() % [[[dataSource objectAtIndex:selectedCategory] objectForKey:@"verkeersborden"] count];
	}
	else {
		if (selectedCategory == -1) {
			selectedCategory = 0;
			selectedSign = 0;
		}
		else {
			NSDictionary *categoryDictionary = [dataSource objectAtIndex:selectedCategory];
			if(selectedSign < [[categoryDictionary objectForKey:@"verkeersborden"] count] - 1) {
				selectedSign++;
			}
			else {
				selectedSign = 0;
				if (selectedCategory < [dataSource count] - 1) {
					selectedCategory++;
				}
				else {
					selectedCategory = 0;
				}
			}
		}
	}
}	

- (void)startProgressbar {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *numberOfSecondsToShowQuestionForSign = [defaults stringForKey:@"numberOfSecondsToShowQuestionForSign"];
    NSString *numberOfSecondsToShowAnswerForSign = [defaults stringForKey:@"numberOfSecondsToShowAnswerForSign"];
    
    signProgressbarTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateProgressbar) userInfo:nil repeats:YES];
    
    if(showingQuestion) {
        [signProgressbar setProgress: 0.0f];
        signProgressbarStep = 1/[numberOfSecondsToShowQuestionForSign floatValue];
    }
    else {
        [signProgressbar setProgress: 1.0f];
        signProgressbarStep = 1/[numberOfSecondsToShowAnswerForSign floatValue];
    }
    
    [signProgressbar setAlpha:1.0f];
    
    gotoNextQuestion = FALSE;
}

- (void)stopProgressbar {
    [signProgressbar setAlpha:0.0f];
    [signProgressbarTimer invalidate];
    signProgressbarTimer = nil;
    
    [self hideInformation];
}

- (void)hideInformation {
    [signImage setAlpha:0.0f];
    [signCategory setAlpha:1.0f];
    [signDescription setAlpha:1.0f];
    
    [UIView animateWithDuration:0.25f animations:^{
        [signImage setAlpha:0.0f];
        [signCategory setAlpha:0.0f];
        [signDescription setAlpha:0.0f];
    } completion:^(BOOL finished){
        if(showingQuestion) {
            [self setAnswerForLabels];
        }
        else {
            [self setQuestionForLabels];
        }
    }];
}

- (void)showInformation {
    [signImage setAlpha:0.0f];
    [signCategory setAlpha:0.0f];
    [signDescription setAlpha:0.0f];
    
    [UIView animateWithDuration:0.25f animations:^{
        [signImage setAlpha:1.0f];
        [signCategory setAlpha:1.0f];
        [signDescription setAlpha:1.0f];
    } completion:^(BOOL finished){
        [self startProgressbar];
    }];
}

- (void)setQuestionForLabels {
    showingQuestion = TRUE;

    NSDictionary *categoryDictionary = [dataSource objectAtIndex:selectedCategory];
    
    NSString *description = [[[categoryDictionary objectForKey:@"verkeersborden"] objectAtIndex:selectedSign] objectForKey:@"verkeersbord"];
    NSRange range = [description rangeOfString:@" "];
    
    [signImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", [[description substringToIndex:range.location] lowercaseString]]]];

    NSString *label = [[[categoryDictionary objectForKey:@"verkeersborden"] objectAtIndex:selectedSign] objectForKey:@"verkeersbord"];
    range = [label rangeOfString:@" "];
    
    [signCategory setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [signCategory setTextAlignment:NSTextAlignmentCenter];
    [signCategory setLineBreakMode: NSLineBreakByWordWrapping];
    [signCategory setNumberOfLines:0];
    [signCategory setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [signCategory setText:@" "];
    
    [signDescription setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [signDescription setTextAlignment:NSTextAlignmentCenter];
    [signDescription setLineBreakMode: NSLineBreakByTruncatingTail];
    [signDescription setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [signDescription setText:NSLocalizedString(@"TrafficSignExamQuestion", @"TrafficSignExamQuestion")];
    
    [self showInformation];
}

- (void)setAnswerForLabels {
    showingQuestion = FALSE;
    
    NSDictionary *categoryDictionary = [dataSource objectAtIndex:selectedCategory];
    
    NSString *description = [[[categoryDictionary objectForKey:@"verkeersborden"] objectAtIndex:selectedSign]objectForKey:@"verkeersbord"];
    NSRange range = [description rangeOfString:@" "];
    
    [signImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", [[description substringToIndex:range.location] lowercaseString]]]];
    
    NSString *label = [[[categoryDictionary objectForKey:@"verkeersborden"] objectAtIndex:selectedSign]objectForKey:@"verkeersbord"];
    range = [label rangeOfString:@" "];
    
    [signCategory setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [signCategory setTextAlignment:NSTextAlignmentCenter];
    [signCategory setLineBreakMode: NSLineBreakByWordWrapping];
    [signCategory setNumberOfLines:0];
    [signCategory setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [signCategory setText:[NSString stringWithFormat:@"%@ (%@)", [categoryDictionary objectForKey:@"categorie"], [NSString stringWithFormat:@"%@", [description substringToIndex:range.location]]]];
    
    [signDescription setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [signDescription setTextAlignment:NSTextAlignmentCenter];
    [signDescription setLineBreakMode: NSLineBreakByWordWrapping];
    [signDescription setNumberOfLines:0];
    [signDescription setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [signDescription setText:[label substringFromIndex:range.location]];
    
    [self showInformation];
}

- (IBAction)viewTapped:(UITapGestureRecognizer*) recognizer {
    if (!gotoNextQuestion) {
        gotoNextQuestion = TRUE;

        [self gotoNext];
    }
}

- (void)gotoNext {
    if(showingQuestion) {
        [self stopProgressbar];
    }
    else {
        [self selectCategoryAndSign];
        
        [self stopProgressbar];
    }
}

- (void)releaseIBOutlets {
	self.signImage = nil;
	self.signCategory = nil;
	self.signDescription = nil;
	self.signProgressbar = nil;
}

@end
