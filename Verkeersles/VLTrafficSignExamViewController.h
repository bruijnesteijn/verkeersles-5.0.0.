//
//  VLTrafficSignExamViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLTrafficSignExamViewController : UIViewController {
	NSArray *dataSource;
	
	IBOutlet UIImageView *signImage;
	IBOutlet UILabel *signCategory;
	IBOutlet UILabel *signDescription;

	IBOutlet UIProgressView *signProgressbar;
	float signProgressbarStep;
    NSTimer *signProgressbarTimer;
	
	int selectedCategory;
	int selectedSign;
	
    BOOL showingQuestion;
    BOOL gotoNextQuestion;
}

@property (nonatomic, retain) NSArray *dataSource;

@property (nonatomic, retain) IBOutlet UIImageView *signImage;
@property (nonatomic, retain) IBOutlet UILabel *signCategory;
@property (nonatomic, retain) IBOutlet UILabel *signDescription;
@property (nonatomic, retain) IBOutlet UIProgressView *signProgressbar;

@end
