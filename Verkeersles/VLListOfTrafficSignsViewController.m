//
//  VLListOfTrafficSignsViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLListOfTrafficSignsViewController.h"
#import "VLListOfTrafficSignsPageContentViewController.h"
#import "VLLocalizationUtility.h"
#import "VLConstants.h"

@interface VLListOfTrafficSignsViewController ()

- (void)initializeDataSource;

@end

@implementation VLListOfTrafficSignsViewController

@synthesize dataSource, selectedTrafficSigns, category, pageViewController;

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initializeDataSource];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VLListOfTrafficSignsPageViewController"];
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    
    VLListOfTrafficSignsPageContentViewController *firstViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[firstViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if ([selectedTrafficSigns count] > 0) {
        [self.navigationItem setTitle:NSLocalizedString(@"Matching Traffic Signs", @"Matching Traffic Signs")];
    }
    else {
        NSString *categoryText = [NSString stringWithFormat:@"%@", [self.dataSource objectForKey:@"categorie"]];
        
        [self.navigationItem setTitle:categoryText];
    }
    
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    VLListOfTrafficSignsPageContentViewController *firstViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[firstViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    if ([selectedTrafficSigns count] > 0) {
        [self.navigationItem setTitle:NSLocalizedString(@"Matching Traffic Signs", @"Matching Traffic Signs")];
    }
    else {
        NSString *categoryText = [NSString stringWithFormat:@"%@", [self.dataSource objectForKey:@"categorie"]];
        
        [self.navigationItem setTitle:categoryText];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (VLListOfTrafficSignsPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([[self.dataSource  objectForKey:@"verkeersborden"] count] == 0) || (index >= [[self.dataSource  objectForKey:@"verkeersborden"] count])) {
        return nil;
    }
    
    VLListOfTrafficSignsPageContentViewController *listOfSignsPageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VLListOfTrafficSignsPageContentViewController"];
    
    NSString *label = [NSString stringWithString:[[[self.dataSource objectForKey:@"verkeersborden"] objectAtIndex:(NSInteger) index] objectForKey:@"verkeersbord"]];
    NSRange range = [label rangeOfString:@" "];

    if ([selectedTrafficSigns count] > 0) {
        NSString *categoryText = [NSString stringWithFormat:@"%@ (%@)", [[[self.dataSource objectForKey:@"verkeersborden"] objectAtIndex:(NSInteger) index] objectForKey:@"categorie"], [NSString stringWithFormat:@"%@", [label substringToIndex:range.location]]];
        
        [listOfSignsPageContentViewController setCategoryLabelText:categoryText];
        
        [self.navigationItem setTitle:NSLocalizedString(@"Matching Traffic Signs", @"Matching Traffic Signs")];
    }
    else {
        NSString *categoryText = [NSString stringWithFormat:@"%@ (%@)", [self.dataSource objectForKey:@"categorie"], [NSString stringWithFormat:@"%@", [label substringToIndex:range.location]]];
        
        [listOfSignsPageContentViewController setCategoryLabelText:categoryText];
    }
    listOfSignsPageContentViewController.signLabelText = [NSString stringWithFormat:@"%@", [label substringFromIndex:range.location]];
    
    NSString *image = [[[self.dataSource objectForKey:@"verkeersborden"] objectAtIndex:index] objectForKey:@"verkeersbord"];
    NSString *filename = [NSString stringWithFormat:@"%@", [[image substringToIndex:range.location] lowercaseString]];
    listOfSignsPageContentViewController.signImageName = filename;

    listOfSignsPageContentViewController.numberOfPages = [[self.dataSource  objectForKey:@"verkeersborden"] count];
    listOfSignsPageContentViewController.currentPageNumber = index;

    return listOfSignsPageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(VLListOfTrafficSignsPageContentViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((VLListOfTrafficSignsPageContentViewController *) viewController).currentPageNumber;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;

    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(VLListOfTrafficSignsPageContentViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((VLListOfTrafficSignsPageContentViewController *) viewController).currentPageNumber;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index == [[self.dataSource  objectForKey:@"verkeersborden"] count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

#pragma mark -
#pragma mark Table view delegate

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = nil;
    
    if ([selectedTrafficSigns count] > 0) {
        self.dataSource = [selectedTrafficSigns objectAtIndex:0];
    }
    else {
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"onlyRVVSigns"] isEqualToString:@"YES"]) {
            plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSignRVV_datasource", @"VLTrafficSignRVV_datasource")];
        }
        else {
            plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSign_datasource", @"VLTrafficSign_datasource")];
        }
        
        self.dataSource = [[NSArray arrayWithContentsOfFile:plistPath] objectAtIndex:category];
    }
}

@end
