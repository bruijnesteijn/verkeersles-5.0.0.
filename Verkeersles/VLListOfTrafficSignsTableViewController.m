//
//  VLListOfSignsTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/20/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import "VLListOfTrafficSignsTableViewController.h"
#import "VLListOfTrafficSignsViewController.h"
#import "VLListOfTrafficSignsTableViewCell.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfTrafficSignsTableViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLListOfTrafficSignsTableViewController

@synthesize dataSource, category, previous, current;

#pragma mark -
#pragma mark View lifecycle

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];
    
    [[self navigationItem] setTitle:[self.dataSource objectForKey:@"categorie"]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:[self.dataSource objectForKey:@"categorie"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Table view data source

- (VLListOfTrafficSignsTableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLSignCell";
    
    VLListOfTrafficSignsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];

    return cell;
}

- (void)configureCell:(VLListOfTrafficSignsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *text = [[[self.dataSource objectForKey:@"verkeersborden"] objectAtIndex:indexPath.row] objectForKey:@"verkeersbord"];
    
    [self setTitleForCell:cell labelText:[NSString stringWithFormat:@"%@", text]];
    [self setImageForCell:cell imageName:[NSString stringWithFormat:@"%@", text]];
}

- (void)setTitleForCell:(VLListOfTrafficSignsTableViewCell *)cell labelText:(NSString *)text {
    NSRange range = [text rangeOfString:@" "];

    [cell.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.titleLabel setNumberOfLines:0];
    [cell.titleLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.titleLabel setText:[NSString stringWithFormat:@"%@", [text substringFromIndex:range.location+1]]];
}

- (void)setImageForCell:(VLListOfTrafficSignsTableViewCell *)cell imageName:(NSString *)text {
    NSRange range = [text rangeOfString:@" "];

    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@", [[text substringToIndex:range.location] lowercaseString]]];
    [cell.image setImage:img];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellAtIndexPath:indexPath];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.dataSource objectForKey:@"verkeersborden"] count];
}

#pragma mark -
#pragma mark Table view delegate

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = nil;
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"onlyRVVSigns"] isEqualToString:@"YES"]) {
        plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSignRVV_datasource", @"VLTrafficSignRVV_datasource")];
    }
    else {
        plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSign_datasource", @"VLTrafficSign_datasource")];
    }
    self.dataSource = [[NSArray arrayWithContentsOfFile:plistPath] objectAtIndex:category];
    
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showListOfTrafficSignsForCategoryPageView"]) {
        VLListOfTrafficSignsViewController *gallery = segue.destinationViewController;
        gallery.category = category;
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
}

@end



