//
//  VLSignSettingsViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSignSettingsViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UILabel *lblShowSignRVV;
	IBOutlet UISwitch *showSignRVV;
    
    IBOutlet UILabel *lblShowSignRandom;
    IBOutlet UISwitch *showSignRandom;
    
    IBOutlet UILabel *lblShowSignImageTimer;
	IBOutlet UITextField *showSignImageTimer;
    
    IBOutlet UILabel *lblShowSignAnswerTimer;
	IBOutlet UITextField *showSignAnswerTimer;
    
    IBOutlet UILabel *lblShowQuizQuestionTimer;
	IBOutlet UITextField *showQuizQuestionTimer;
    
    IBOutlet UILabel *lblShowQuizImageTimer;
	IBOutlet UITextField *showQuizImageTimer;
    
    IBOutlet UILabel *lblShowQuizAnswerTimer;
	IBOutlet UITextField *showQuizAnswerTimer;
    
    IBOutlet UILabel *lblShowQuizErrorTimer;
	IBOutlet UITextField *showQuizErrorTimer;

    IBOutlet UILabel *lblTitle1;
    IBOutlet UILabel *lblTitle2;
    IBOutlet UILabel *lblSeconds1;
    IBOutlet UILabel *lblSeconds2;
    IBOutlet UILabel *lblSeconds3;
    IBOutlet UILabel *lblSeconds4;
    IBOutlet UILabel *lblSeconds5;
    IBOutlet UILabel *lblSeconds6;

	BOOL viewIsMoved;
}

@property (nonatomic, retain) IBOutlet UILabel *lblShowSignRVV;
@property (nonatomic, retain) IBOutlet UILabel *lblShowSignRandom;
@property (nonatomic, retain) IBOutlet UILabel *lblShowSignImageTimer;
@property (nonatomic, retain) IBOutlet UILabel *lblShowSignAnswerTimer;
@property (nonatomic, retain) IBOutlet UILabel *lblShowQuizQuestionTimer;
@property (nonatomic, retain) IBOutlet UILabel *lblShowQuizImageTimer;
@property (nonatomic, retain) IBOutlet UILabel *lblShowQuizAnswerTimer;
@property (nonatomic, retain) IBOutlet UILabel *lblShowQuizErrorTimer;
@property (nonatomic, retain) IBOutlet UILabel *lblTitle1;
@property (nonatomic, retain) IBOutlet UILabel *lblTitle2;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds1;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds2;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds3;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds4;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds5;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds6;

@property (nonatomic, retain) IBOutlet UISwitch *showSignRVV;
@property (nonatomic, retain) IBOutlet UISwitch *showSignRandom;
@property (nonatomic, retain) IBOutlet UITextField *showSignImageTimer;
@property (nonatomic, retain) IBOutlet UITextField *showSignAnswerTimer;
@property (nonatomic, retain) IBOutlet UITextField *showQuizQuestionTimer;
@property (nonatomic, retain) IBOutlet UITextField *showQuizImageTimer;
@property (nonatomic, retain) IBOutlet UITextField *showQuizAnswerTimer;
@property (nonatomic, retain) IBOutlet UITextField *showQuizErrorTimer;

//-(IBAction)moveView;

@end
