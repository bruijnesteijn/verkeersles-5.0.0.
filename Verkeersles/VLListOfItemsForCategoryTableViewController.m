//
//  VLListOfItemsForCategoryTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 2/19/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import "VLListOfItemsForCategoryTableViewController.h"
#import "VLListOfItemsForCategoryTableViewHeaderCell.h"
#import "VLListOfItemsForCategoryTableViewCell.h"
#import "VLAppDelegate.h"
#import "VLTheoryCategory.h"
#import "VLTheoryItem.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfItemsForCategoryTableViewController(Private)

- (void)selectedOtherSection:(UIGestureRecognizer *)sender;
- (void)refreshTableView;
- (void)queryDB;
- (void)setupNavigationBarTitle;

@end

@implementation VLListOfItemsForCategoryTableViewController

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize category, searchOneCategoryPredicate, searchAllCategoriesPredicate, searchAllCategories;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    index = 0;
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedSectionHeaderHeight = 44.0;
    self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    
    self.searchOneController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchOneController.searchResultsUpdater = self;
    self.searchOneController.dimsBackgroundDuringPresentation = NO;
    self.searchOneController.delegate = self;
    self.searchOneController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchOneController.searchBar;
    
    self.searchOneCategoryPredicate = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self.searchOneController.searchBar setPlaceholder:NSLocalizedString(@"SearchInCurrentSelection", @"SearchInCurrentSelection")];
    self.searchOneCategoryPredicate = nil;

    [self refreshTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self refreshTableView];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [self.searchOneController.view removeFromSuperview];
}

#pragma mark - UISearchControllerDelegate

-(void)willPresentSearchController:(UISearchController *)searchController {
    [self.searchOneController.searchBar setPlaceholder:NSLocalizedString(@"SearchInCurrentSelection", @"SearchInCurrentSelection")];
    self.searchOneCategoryPredicate = @"";
    self.searchOneController.searchBar.text = @"";
}

-(void)didPresentSearchController:(UISearchController *)searchController {
}

-(void)willDismissSearchController:(UISearchController *)searchController {
}

-(void)didDismissSearchController:(UISearchController *)searchController {
    [self.searchOneController.searchBar setPlaceholder:NSLocalizedString(@"SearchInCurrentSelection", @"SearchInCurrentSelection")];
    self.searchOneCategoryPredicate = nil;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchOneController.searchBar setPlaceholder:NSLocalizedString(@"SearchInCurrentSelection", @"SearchInCurrentSelection")];
    self.searchOneCategoryPredicate = nil;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchOneController setActive:NO];
    
    [self refreshTableView];
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text;
    if ([searchText length] != 0) {
        NSString *lastChar = [searchText substringFromIndex:searchText.length - 1];
        self.searchOneCategoryPredicate = [self.searchOneCategoryPredicate stringByAppendingString:lastChar];
    }
}

#pragma mark -
#pragma mark Table View Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView {
    return [[self.fetchedResultsController fetchedObjects] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = 0;
    
    if (section == index) {
        rows = 1;
    }
    
    return rows;
}

- (VLListOfItemsForCategoryTableViewHeaderCell *) headerInSection:(NSInteger)section {
    static NSString *cellIdentifier = @"VLTheoryItemHeaderCell";
    
    VLListOfItemsForCategoryTableViewHeaderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [self configureHeaderCell:cell forSection:section];
    
    if((section % 2) == 0 ) {
        [cell setBackgroundColor:TABLEVIEW_SECTION_DARK_COLOR];
    }
    else {
        [cell setBackgroundColor:TABLEVIEW_SECTION_LIGHT_COLOR];
    }
    [cell.backgroundView setAlpha:0.6];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureHeaderCell:(VLListOfItemsForCategoryTableViewHeaderCell *)cell forSection:(NSInteger)section {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
    NSManagedObject *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *question = [NSString stringWithFormat:@"%@", [managedObject valueForKey:NSLocalizedString(@"TheoryItemQuestion", @"TheoryItemQuestion")]];
    
    [self setQuestionForHeaderCell:cell labelText:question forSection:section];
}

- (void)setQuestionForHeaderCell:(VLListOfItemsForCategoryTableViewHeaderCell *)cell labelText:(NSString *)text forSection:(NSInteger)section {
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedOtherSection:)];
    
    [cell.questionLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [cell.questionLabel setNumberOfLines:0];
    [cell.questionLabel setTextColor:TABLEVIEW_SECTION_TEXTCOLOR];
    [cell.questionLabel setText:text];
    [cell.questionLabel setUserInteractionEnabled:YES];
    [cell.questionLabel addGestureRecognizer:recognizer];
    [cell.questionLabel setTag:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [self headerInSection:section];
}

- (VLListOfItemsForCategoryTableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLTheoryItemCell";
    
    VLListOfItemsForCategoryTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(VLListOfItemsForCategoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSManagedObject *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *answer = [NSString stringWithFormat:@"%@", [managedObject valueForKey:NSLocalizedString(@"TheoryItemAnswer", @"TheoryItemAnswer")]];
    
    [self setAnswerForCell:cell labelText:answer];
}

- (void)setAnswerForCell:(VLListOfItemsForCategoryTableViewCell *)cell labelText:(NSString *)text {
    [cell.answerLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.answerLabel setNumberOfLines:0];
    [cell.answerLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.answerLabel setText:text];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellAtIndexPath:indexPath];
}

#pragma mark -
#pragma mark Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"VLTheoryItem" inManagedObjectContext:[self.category managedObjectContext]];
    
    NSString *sectionKey = nil;
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"theoryItemAnswer" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    sectionKey = @"theoryItemAnswer";
    
    if(self.searchOneCategoryPredicate == nil) {
        self.searchOneCategoryPredicate = @"";
    }
    
    NSPredicate *predicate = nil;
    if (self.searchAllCategories) {
        if ([self.searchAllCategoriesPredicate isEqualToString:@""] && [self.searchOneCategoryPredicate isEqualToString:@""]) {
                predicate =	[NSPredicate predicateWithFormat:@""];
        }
        else if (![self.searchAllCategoriesPredicate isEqualToString:@""] && [self.searchOneCategoryPredicate isEqualToString:@""]){
            predicate =	[NSPredicate predicateWithFormat:NSLocalizedString(@"PredicateAllCategoriesWithoutSelection", @"PredicateAllCategoriesWithoutSelection"), self.searchAllCategoriesPredicate, self.searchAllCategoriesPredicate];
        }
        else if ([self.searchAllCategoriesPredicate isEqualToString:@""] && ![self.searchOneCategoryPredicate isEqualToString:@""]){
            predicate =	[NSPredicate predicateWithFormat:NSLocalizedString(@"PredicateSelectionForAllCategories", @"PredicateSelectionForAllCategories"), self.searchOneCategoryPredicate, self.searchOneCategoryPredicate];
        }
        else {
            predicate =	[NSPredicate predicateWithFormat:NSLocalizedString(@"PredicateDetailedSelectionForAllCategories", @"PredicateDetailedSelectionForAllCategories"), self.searchOneCategoryPredicate, self.searchOneCategoryPredicate, self.searchAllCategoriesPredicate, self.searchAllCategoriesPredicate];
        }
    }
    else {
        if ([self.searchAllCategoriesPredicate isEqualToString:@""] && [self.searchOneCategoryPredicate isEqualToString:@""]) {
            predicate =	[NSPredicate predicateWithFormat:@"belongsToVLTheoryCategory == %@", self.category];
        }
        else if (![self.searchAllCategoriesPredicate isEqualToString:@""] && [self.searchOneCategoryPredicate isEqualToString:@""]){
            predicate =	[NSPredicate predicateWithFormat:NSLocalizedString(@"PredicateOneCategoryWithSelection", @"PredicateOneCategoryWithSelection"), self.category, self.searchAllCategoriesPredicate, self.searchAllCategoriesPredicate];
        }
        else if ([self.searchAllCategoriesPredicate isEqualToString:@""] && ![self.searchOneCategoryPredicate isEqualToString:@""]){
            predicate =	[NSPredicate predicateWithFormat:NSLocalizedString(@"PredicateOneCategoryWithSelection", @"PredicateOneCategoryWithSelection"), self.category, self.searchOneCategoryPredicate, self.searchOneCategoryPredicate];
        }
        else {
            predicate =	[NSPredicate predicateWithFormat:NSLocalizedString(@"PredicateOneCategoryWithDetailedSelection", @"PredicateOneCategoryWithDetailedSelection"), self.category, self.searchOneCategoryPredicate, self.searchOneCategoryPredicate, self.searchAllCategoriesPredicate, self.searchAllCategoriesPredicate];
        }
    }

    [fetchRequest setPredicate:predicate];
    
    [NSFetchedResultsController deleteCacheWithName:@"VLTheoryItem"];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:[self.category managedObjectContext]
                                                                            sectionNameKeyPath:sectionKey
                                                                                     cacheName:@"VLTheoryItem"];
    frc.delegate = self;
    _fetchedResultsController = frc;
    
    return _fetchedResultsController;
}

@end

@implementation VLListOfItemsForCategoryTableViewController(Private)

- (void)refreshTableView {
    [self queryDB];
    
    [self setupNavigationBarTitle];
    
    [self.tableView reloadData];
}

- (void)setupNavigationBarTitle {
    if (searchAllCategories) {
        [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ (%d)", NSLocalizedString(@"All Categories", @"All Categories"), (int) [[self.fetchedResultsController fetchedObjects] count]]];
    }
    else {
        [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ (%d)", [category valueForKey:NSLocalizedString(@"TheoryCategoryName", @"TheoryCategoryName")], (int) [[self.fetchedResultsController fetchedObjects] count]]];
    }

    [[[[self navigationController] navigationBar] backItem] setTitle:NSLocalizedString(@"TheoryTitle", @"TheoryTitle")];
}

- (void)queryDB {
    NSError *error = nil;
    
    _fetchedResultsController = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ErrorLoadingDatabaseTitle", @"ErrorLoadingDatabaseTitle") message:NSLocalizedString(@"ErrorLoadingDatabaseMessage", @"ErrorLoadingDatabaseMessage") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *continueWithoutAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }];
        
        [alert addAction:continueWithoutAction];
    }
}

- (void)selectedOtherSection:(UIGestureRecognizer *)sender {
    UILabel *label = (UILabel *) sender.view;
    index = [label tag];
    
    [self.tableView reloadData];

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];

    [self.tableView scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionTop
                                 animated:NO];
}

@end

