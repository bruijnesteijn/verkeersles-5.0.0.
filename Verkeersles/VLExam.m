//
//  VLExam.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 15/12/15.
//  Copyright © 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLExam.h"

@implementation VLExam

@synthesize examID, examNumberOfQuestions, examNumberOfQuestionsAnswered, examNumberOfQuestionsAnsweredCorrect;

- (instancetype)init {
    self = [super init];
    
    return self;
}

@end