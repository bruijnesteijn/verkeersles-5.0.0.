//
//  VLTrafficSignsSettingsViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import "VLTrafficSignsSettingsViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLTrafficSignsSettingsViewController(Private)

- (void)saveSettings;
- (void)releaseIBOutlets;

@end

@implementation VLTrafficSignsSettingsViewController

@synthesize lblOnlyRVVSigns ,lblShowSignsInRandomOrder, lblNumberOfSecondsToShowQuestionForSign, lblNumberOfSecondsToShowAnswerForSign, lblSeconds1, lblSeconds2, onlyRVVSigns, showSignsInRandomOrder, numberOfSecondsToShowQuestionForSign, numberOfSecondsToShowAnswerForSign;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [lblOnlyRVVSigns setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblOnlyRVVSigns.text = NSLocalizedString(@"OnlyRVVSigns", @"OnlyRVVSigns");
	if ([[defaults stringForKey:@"onlyRVVSigns"] isEqualToString:@"YES"]) {
		onlyRVVSigns.on = YES;
	}
	else {
		onlyRVVSigns.on = NO;
	}

    [lblShowSignsInRandomOrder setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblShowSignsInRandomOrder.text = NSLocalizedString(@"ShowSignsInRandomOrder", @"ShowSignsInRandomOrder");
	if ([[defaults stringForKey:@"showSignsInRandomOrder"] isEqualToString:@"YES"]) {
		showSignsInRandomOrder.on = YES;
	}
	else {
		showSignsInRandomOrder.on = NO;
	}

    [lblNumberOfSecondsToShowQuestionForSign setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblNumberOfSecondsToShowQuestionForSign.text = NSLocalizedString(@"NumberOfSecondsToShowQuestionForSign", @"NumberOfSecondsToShowQuestionForSign");
    numberOfSecondsToShowQuestionForSign.text = [defaults stringForKey:@"numberOfSecondsToShowQuestionForSign"];

    [lblNumberOfSecondsToShowAnswerForSign setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblNumberOfSecondsToShowAnswerForSign.text = NSLocalizedString(@"NumberOfSecondsToShowAnswerForSign", @"NumberOfSecondsToShowAnswerForSign");
    numberOfSecondsToShowAnswerForSign.text = [defaults stringForKey:@"numberOfSecondsToShowAnswerForSign"];

    [lblSeconds1 setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblSeconds1.text = NSLocalizedString(@"Seconden", @"Seconden");

    [lblSeconds2 setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblSeconds2.text = NSLocalizedString(@"Seconden", @"Seconden");

    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficSignPractiseSettingsTitle", @"TrafficSignPractiseSettingsTitle")];
    
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self saveSettings];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self.tableView reloadData];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficSignPractiseSettingsTitle", @"TrafficSignPractiseSettingsTitle")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
	[self releaseIBOutlets];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle;
    
    switch (section)
    {
        case 0:
            sectionTitle = NSLocalizedString(@"TrafficSignPractiseSettingsSelectionTitle", @"TrafficSignPractiseSettingsSelectionTitle");
            
            break;
            
        case 1:
            sectionTitle = NSLocalizedString(@"TrafficSignPractiseSettingsTimersTitle", @"TrafficSignPractiseSettingsTimersTitle");
            
            break;

        default:
            sectionTitle = @"";
            
            break;
    }
    
    return sectionTitle;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *) view;
    [header.contentView setBackgroundColor:TABLEVIEW_SECTION_COLOR];
    [header.contentView setAlpha:0.6];
    [header.textLabel setTextColor:TABLEVIEW_SECTION_TEXTCOLOR];
    [header.textLabel setOpaque:NO];
    [header.textLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

@end

@implementation VLTrafficSignsSettingsViewController(Private)

#pragma mark -
#pragma mark Settings

- (void)saveSettings {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (onlyRVVSigns.on) {
        [defaults setObject:@"YES" forKey:@"onlyRVVSigns"];
    }
    else {
        [defaults setObject:@"NO" forKey:@"onlyRVVSigns"];
    }
    
    if (showSignsInRandomOrder.on) {
        [defaults setObject:@"YES" forKey:@"showSignsInRandomOrder"];
    }
    else {
        [defaults setObject:@"NO" forKey:@"showSignsInRandomOrder"];
    }
    
    if([[numberOfSecondsToShowQuestionForSign text] intValue] > 0){
        [defaults setObject:[numberOfSecondsToShowQuestionForSign text] forKey:@"numberOfSecondsToShowQuestionForSign"];
    }
    else {
        numberOfSecondsToShowQuestionForSign.text = [defaults stringForKey:@"numberOfSecondsToShowQuestionForSign"];
    }
    
    if([[numberOfSecondsToShowAnswerForSign text] intValue] > 0){
        [defaults setObject:[numberOfSecondsToShowAnswerForSign text] forKey:@"numberOfSecondsToShowAnswerForSign"];
    }
    else {
        numberOfSecondsToShowAnswerForSign.text = [defaults stringForKey:@"numberOfSecondsToShowAnswerForSign"];
    }
    
    [numberOfSecondsToShowQuestionForSign resignFirstResponder];
    [numberOfSecondsToShowAnswerForSign resignFirstResponder];
}

- (void)releaseIBOutlets {
    self.lblOnlyRVVSigns = nil;
	self.lblShowSignsInRandomOrder = nil;
	self.lblNumberOfSecondsToShowQuestionForSign = nil;
	self.lblNumberOfSecondsToShowAnswerForSign = nil;
    
    self.onlyRVVSigns = nil;
    self.showSignsInRandomOrder = nil;
    self.numberOfSecondsToShowQuestionForSign = nil;
    self.numberOfSecondsToShowAnswerForSign = nil;
    
    self.lblSeconds1 = nil;
    self.lblSeconds2 = nil;
}

@end
