//
//  VLTheorySettingsViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLTheorySettingsViewController : UITableViewController <UITextFieldDelegate> {
    IBOutlet UILabel *lblShowImagesWithText;
    IBOutlet UISwitch *showImagesWithText;

    IBOutlet UILabel *lblNumberOfSecondsToShowQuestion;
	IBOutlet UITextField *numberOfSecondsToShowQuestion;
    IBOutlet UILabel *lblNumberOfSecondsToAnswerTheQuestion;
	IBOutlet UITextField *numberOfSecondsToAnswerTheQuestion;
    IBOutlet UILabel *lblNumberOfSecondsToShowAnswer;
	IBOutlet UITextField *numberOfSecondsToShowAnswer;
    IBOutlet UILabel *lblNumberOfSecondsToShowError;
	IBOutlet UITextField *numberOfSecondsToShowError;

    IBOutlet UILabel *lblSeconds1;
    IBOutlet UILabel *lblSeconds2;
    IBOutlet UILabel *lblSeconds3;
    IBOutlet UILabel *lblSeconds4;
}

@property (nonatomic, retain) IBOutlet UILabel *lblShowImagesWithText;
@property (nonatomic, retain) IBOutlet UISwitch *showImagesWithText;

@property (nonatomic, retain) IBOutlet UILabel *lblNumberOfSecondsToShowQuestion;
@property (nonatomic, retain) IBOutlet UILabel *lblNumberOfSecondsToAnswerTheQuestion;
@property (nonatomic, retain) IBOutlet UILabel *lblNumberOfSecondsToShowAnswer;
@property (nonatomic, retain) IBOutlet UILabel *lblNumberOfSecondsToShowError;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds1;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds2;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds3;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds4;

@property (nonatomic, retain) IBOutlet UITextField *numberOfSecondsToShowQuestion;
@property (nonatomic, retain) IBOutlet UITextField *numberOfSecondsToAnswerTheQuestion;
@property (nonatomic, retain) IBOutlet UITextField *numberOfSecondsToShowAnswer;
@property (nonatomic, retain) IBOutlet UITextField *numberOfSecondsToShowError;

@end
