//
//  VLLabel.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 17/07/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLLabel : UILabel

@end
