//
//  VLSettingsTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 17/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLSettingsTableViewController.h"
#import "VLTrafficSignsSettingsViewController.h"
#import "VLTheorySettingsViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLSettingsTableViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLSettingsTableViewController

@synthesize dataSource;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self initializeDataSource];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Instellingen", @"Instellingen")];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier;
    
    if(indexPath.row == 0) {
        cellIdentifier = @"VLSettingsForSignsExam";
    }
    else if (indexPath.row == 1){
        cellIdentifier = @"VLSettingsForTheoryExam";
    }
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.textLabel setText:NSLocalizedString([dataSource objectAtIndex:indexPath.row], [dataSource objectAtIndex:indexPath.row])];
}

#pragma mark -
#pragma mark Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if ([segue.identifier isEqualToString:@"showSettingsForSignsExam"]) {
        VLTrafficSignsSettingsViewController *trafficSignsSettings = segue.destinationViewController;
        trafficSignsSettings.title = [NSString stringWithString:[dataSource objectAtIndex:indexPath.row]];
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
    
    if ([segue.identifier isEqualToString:@"showSettingsForTheoryExam"]) {
        VLTheorySettingsViewController *theorySettings = segue.destinationViewController;
        theorySettings.title = [NSString stringWithString:[dataSource objectAtIndex:indexPath.row]];
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
}


@end

@implementation VLSettingsTableViewController(Private)

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = nil;

    plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLAppSettings_datasource", @"VLAppSettings_datasource")];

    self.dataSource = [NSArray arrayWithContentsOfFile:plistPath];
    
    [self.tableView reloadData];
}

@end
