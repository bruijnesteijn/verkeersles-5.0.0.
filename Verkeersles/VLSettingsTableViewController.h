//
//  VLSettingsTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 17/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSettingsTableViewController : UITableViewController {
    NSArray *dataSource;
}

@property (nonatomic, retain) NSArray *dataSource;

@end
