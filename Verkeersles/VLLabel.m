//
//  VLLabel.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 17/07/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLLabel.h"

@interface VLLabel ()

@end

@implementation VLLabel

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
  
    if (self.numberOfLines == 0 && bounds.size.width != self.preferredMaxLayoutWidth) {
        self.preferredMaxLayoutWidth = self.bounds.size.width;
        [self setNeedsUpdateConstraints];
    }
}

@end
