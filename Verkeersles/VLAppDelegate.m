//
//  VLAppDelegate.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLAppDelegate.h"
#import "VLListOfTrafficSignCategoriesTableViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLAppDelegate(Private)

- (NSURL *)applicationDocumentsDirectory;
- (void)coreDataError;
- (void)initializeAppSettings;
- (void)initializeTrafficExams;
- (void)initializeAppLanguage;

@end

@implementation VLAppDelegate

@synthesize window, managedObjectContext, managedObjectModel, persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialize custom language.
    [self initializeAppLanguage];
    
    // Initialize settings.
    [self initializeAppSettings];
    
    // Initialize exams.
    [self initializeTrafficExams];
    
    [self.window setTintColor:APP_BLUE_COLOR];

    [[UINavigationBar appearance] setTintColor:APP_BLUE_COLOR];
    [[UIProgressView appearance] setTintColor:PROGRESSBAR_RED_COLOR];

    [window makeKeyAndVisible];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
}

#pragma mark -
#pragma mark Core Data stack

- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"VLTheoryDB" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"VLTheoryDB.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        [self coreDataError];
    }
    
    return persistentStoreCoordinator;
}

@end

@implementation VLAppDelegate(Private)

#pragma mark -
#pragma mark Application's Documents directory

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -
#pragma mark Core Data

- (void)coreDataError {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CoreDataDatabaseErrorTitle", @"CoreDataDatabaseErrorTitle") message:NSLocalizedString(@"CoreDataDatabaseErrorMessage", @"CoreDataDatabaseErrorMessage") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *continueWithoutAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }];
    
    [alert addAction:continueWithoutAction];
    
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

#pragma mark -
#pragma mark Settings

- (void)initializeAppSettings {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSString *onlyRVVSigns = [defaults stringForKey:@"onlyRVVSigns"];
    NSString *showSignsInRandomOrder = [defaults stringForKey:@"showSignsInRandomOrder"];
	NSString *numberOfSecondsToShowQuestionForSign = [defaults stringForKey:@"numberOfSecondsToShowQuestionForSign"];
	NSString *numberOfSecondsToShowAnswerForSign = [defaults stringForKey:@"numberOfSecondsToShowAnswerForSign"];
    
    NSString *showImagesWithText = [defaults stringForKey:@"showImagesWithText"];
	NSString *numberOfSecondsToShowQuestion = [defaults stringForKey:@"numberOfSecondsToShowQuestion"];
	NSString *numberOfSecondsToAnswerTheQuestion = [defaults stringForKey:@"numberOfSecondsToAnswerTheQuestion"];
	NSString *numberOfSecondsToShowAnswer = [defaults stringForKey:@"numberOfSecondsToShowAnswer"];
	NSString *numberOfSecondsToShowError = [defaults stringForKey:@"numberOfSecondsToShowError"];
    
    NSString *language = [VLLocalizationUtility getLanguage];
    
    if (showImagesWithText == nil)
    {
        [defaults setObject:@"YES" forKey:@"showImagesWithText"];
    }
	if (onlyRVVSigns == nil)
	{
		[defaults setObject:@"NO" forKey:@"onlyRVVSigns"];
	}
    if (showSignsInRandomOrder == nil)
    {
        [defaults setObject:@"YES" forKey:@"showSignsInRandomOrder"];
    }
	if (numberOfSecondsToShowQuestionForSign == nil)
	{
		[defaults setObject:@"8" forKey:@"numberOfSecondsToShowQuestionForSign"];
	}
	if (numberOfSecondsToShowAnswerForSign == nil)
	{
		[defaults setObject:@"8" forKey:@"numberOfSecondsToShowAnswerForSign"];
	}
    
	if (numberOfSecondsToShowQuestion == nil)
	{
		[defaults setObject:@"8" forKey:@"numberOfSecondsToShowQuestion"];
	}
	if (numberOfSecondsToAnswerTheQuestion == nil)
	{
		[defaults setObject:@"12" forKey:@"numberOfSecondsToAnswerTheQuestion"];
	}
	if (numberOfSecondsToShowAnswer == nil)
	{
		[defaults setObject:@"8" forKey:@"numberOfSecondsToShowAnswer"];
	}
	if (numberOfSecondsToShowError == nil)
	{
		[defaults setObject:@"8" forKey:@"numberOfSecondsToShowError"];
	}
    
	if (language == nil)
	{
        [VLLocalizationUtility setLanguage:@"nl"];
	}
}

#pragma mark -
#pragma mark Exams

- (void)initializeTrafficExams {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *examsInitialized = [defaults stringForKey:@"examsInitialized"];
    if(examsInitialized == nil) {
        NSString *fileExamAnswersIn = [NSString stringWithFormat:@"VLExamAnswers.plist"];
        NSString *filePathIn = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileExamAnswersIn];
        
        for (int i = 1; i < APP_NUMBER_OF_EXAMS + 1; i++) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *fileExamAnswersOut = [NSString stringWithFormat:@"VLExamAnswers_%d_%d.plist", APP_EXAM_ANSWERS_FILENAME_VERSION, i];
            NSString *filePathOut = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileExamAnswersOut];
		
            NSFileManager *fileManager = [NSFileManager defaultManager];
		
            if ([fileManager fileExistsAtPath:filePathOut]) {
                [fileManager removeItemAtPath:filePathOut error:nil];
            }
		
            if (![fileManager fileExistsAtPath:filePathOut]) {
                [fileManager copyItemAtPath:filePathIn toPath:filePathOut error:nil];
            }
        }
    }
}

#pragma mark -
#pragma mark Localization

// Initialize language.
- (void)initializeAppLanguage {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString *selectedLanguage = [defaults objectForKey:@"language"];

    if (!([selectedLanguage isEqualToString:@"nl"] && [selectedLanguage isEqualToString:@"en"])) {
        [VLLocalizationUtility setLanguage:@"nl"];
        
        [defaults setObject:@"nl" forKey:@"language"];
	}
    else if ([selectedLanguage isEqualToString:@"nl"]) {
        [VLLocalizationUtility setLanguage:@"nl"];
	}
	else if ([selectedLanguage isEqualToString:@"en"]) {
        [VLLocalizationUtility setLanguage:@"en"];
	}
}

@end
