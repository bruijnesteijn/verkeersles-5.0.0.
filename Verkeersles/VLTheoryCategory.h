//
//  VLTheoryCategory.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijnon 7/9/11.
//  Copyright (c) 2011 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VLTheoryItem;

@interface VLTheoryCategory : NSManagedObject {
}

@property (nonatomic, retain) NSString * theoryCategoryDescription;
@property (nonatomic, retain) NSString * theoryCategoryIcon;
@property (nonatomic, retain) NSString * theoryCategoryBeschrijving;
@property (nonatomic, retain) NSString * theoryCategoryNaam;
@property (nonatomic, retain) NSString * theoryCategoryID;
@property (nonatomic, retain) NSString * theoryCategoryName;

@property (nonatomic, retain) NSSet* hasVLTheoryItem;

@end
