//
//  VLAnswer.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 15/12/15.
//  Copyright © 2015 Bart Bruijnesteijn. All rights reserved.
//


#import "VLAnswer.h"

@implementation VLAnswer

@synthesize answerExamID, answerID, answerCorrectOption, answerSelectedOption, answerCorrect;

- (instancetype)init {
    self = [super init];
    
    return self;
}

@end
