//
//  VLLanguageTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLAppDelegate.h"
#import "VLLanguageTableViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLLanguageTableViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLLanguageTableViewController

@synthesize dataSource;

- (id)init
{
    return self;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];

    [[self navigationItem] setTitle:NSLocalizedString(@"Taal", @"Taal")];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Taal", @"Taal")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLLanguageCell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.textLabel setText:NSLocalizedString([dataSource objectAtIndex:indexPath.row], [dataSource objectAtIndex:indexPath.row])];
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    if ([[VLLocalizationUtility getLanguage] isEqualToString:@"nl"] && indexPath.row == 0) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    if ([[VLLocalizationUtility getLanguage] isEqualToString:@"en"] && indexPath.row == 1) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (indexPath.row == 0) {
        [VLLocalizationUtility setLanguage:@"nl"];
        [defaults setObject:@"nl" forKey:@"language"];
    }
    if (indexPath.row == 1) {
        [VLLocalizationUtility setLanguage:@"en"];
        [defaults setObject:@"en" forKey:@"language"];
    }
    
    NSIndexPath *nlRow = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cellNl = (UITableViewCell*) [tableView cellForRowAtIndexPath:nlRow];
    [cellNl.textLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cellNl.textLabel setText:NSLocalizedString([dataSource objectAtIndex:0], [dataSource objectAtIndex:0])];

    NSIndexPath *enRow = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *cellEn= (UITableViewCell*) [tableView cellForRowAtIndexPath:enRow];
    [cellEn.textLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cellEn.textLabel setText:NSLocalizedString([dataSource objectAtIndex:1], [dataSource objectAtIndex:1])];

    if (indexPath.row == 0) {
        [cellNl setAccessoryType:UITableViewCellAccessoryCheckmark];
        [cellEn setAccessoryType:UITableViewCellAccessoryNone];
    }
    if (indexPath.row == 1) {
        [cellEn setAccessoryType:UITableViewCellAccessoryCheckmark];
        [cellNl setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Taal", @"Taal")];
    [self setTitle:NSLocalizedString(@"Taal", @"Taal")];
    
    if([@"Meer" isEqualToString:[[[[self navigationController] navigationBar] backItem] title]] || [@"More" isEqualToString:[[[[self navigationController] navigationBar] backItem] title]]) {
        [[[[self navigationController] navigationBar] backItem] setTitle:NSLocalizedString(@"Meer", @"Meer")];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChangedNotification" object:self];
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end

@implementation VLLanguageTableViewController(Private)

- (void)initializeDataSource {
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLLanguages_datasource", @"VLLanguages_datasource")];
    
    self.dataSource = [NSArray arrayWithContentsOfFile:plistPath];
    
    [self.tableView reloadData];
}

@end
