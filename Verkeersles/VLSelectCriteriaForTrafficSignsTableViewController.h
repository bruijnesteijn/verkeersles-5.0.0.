//
//  VLSelectCriteriaForTrafficSignsTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSelectCriteriaForTrafficSignsTableViewController : UITableViewController {
    NSArray *dataSource;

    IBOutlet UIImageView *circleImageView;
    IBOutlet UIImageView *squareImageView;
    IBOutlet UIImageView *diamondImageView;
    IBOutlet UIImageView *rectangleLandscapeImageView;
    IBOutlet UIImageView *rectanglePortraitImageView;
    IBOutlet UIImageView *triangleBottomImageView;
    IBOutlet UIImageView *triangleTopImageView;
    IBOutlet UIButton *blueButton;
    IBOutlet UIButton *redButton;
    IBOutlet UIButton *yellowButton;
    IBOutlet UIButton *greenButton;
    IBOutlet UIButton *blackButton;
    IBOutlet UIButton *numberButton;
    IBOutlet UIButton *letterButton;
    IBOutlet UIButton *textButton;
    IBOutlet UIImageView *carImageView;
    IBOutlet UIImageView *truckImageView;
    IBOutlet UIImageView *motorImageView;
    IBOutlet UIImageView *mopedImageView;
    IBOutlet UIImageView *bicycleImageView;
    IBOutlet UIImageView *personImageView;
    IBOutlet UIImageView *busImageView;
    IBOutlet UIImageView *trainImageView;
    IBOutlet UIImageView *tractorImageView;
    IBOutlet UIImageView *animalImageView;
    IBOutlet UIImageView *arrowImageView;
    IBOutlet UIButton *otherButton;
    
    NSMutableArray *searchCriteria;
    NSMutableDictionary *signsMatchingCriteria;
}

@property (nonatomic, retain) NSArray *dataSource;

@property (nonatomic, retain) IBOutlet UIImageView *circleImageView;
@property (nonatomic, retain) IBOutlet UIImageView *squareImageView;
@property (nonatomic, retain) IBOutlet UIImageView *diamondImageView;
@property (nonatomic, retain) IBOutlet UIImageView *rectangleLandscapeImageView;
@property (nonatomic, retain) IBOutlet UIImageView *rectanglePortraitImageView;
@property (nonatomic, retain) IBOutlet UIImageView *triangleBottomImageView;
@property (nonatomic, retain) IBOutlet UIImageView *triangleTopImageView;
@property (nonatomic, retain) IBOutlet UIButton *blueButton;
@property (nonatomic, retain) IBOutlet UIButton *redButton;
@property (nonatomic, retain) IBOutlet UIButton *yellowButton;
@property (nonatomic, retain) IBOutlet UIButton *greenButton;
@property (nonatomic, retain) IBOutlet UIButton *blackButton;
@property (nonatomic, retain) IBOutlet UIButton *numberButton;
@property (nonatomic, retain) IBOutlet UIButton *letterButton;
@property (nonatomic, retain) IBOutlet UIButton *textButton;
@property (nonatomic, retain) IBOutlet UIImageView *carImageView;
@property (nonatomic, retain) IBOutlet UIImageView *truckImageView;
@property (nonatomic, retain) IBOutlet UIImageView *motorImageView;
@property (nonatomic, retain) IBOutlet UIImageView *mopedImageView;
@property (nonatomic, retain) IBOutlet UIImageView *bicycleImageView;
@property (nonatomic, retain) IBOutlet UIImageView *personImageView;
@property (nonatomic, retain) IBOutlet UIImageView *busImageView;
@property (nonatomic, retain) IBOutlet UIImageView *trainImageView;
@property (nonatomic, retain) IBOutlet UIImageView *tractorImageView;
@property (nonatomic, retain) IBOutlet UIImageView *animalImageView;
@property (nonatomic, retain) IBOutlet UIImageView *arrowImageView;
@property (nonatomic, retain) IBOutlet UIButton *otherButton;

@property (nonatomic, retain) NSMutableArray *searchCriteria;
@property (nonatomic, retain) NSMutableDictionary *signsMatchingCriteria;

@end