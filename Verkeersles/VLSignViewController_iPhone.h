//
//  VLSignViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSignViewController : UIViewController <UIScrollViewDelegate> {
    NSDictionary *categoryDictionary;
    int category;
    
    BOOL signIsChanged;
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIPageControl *pageControl;
    int currentPage;
}

@property (nonatomic, retain) NSDictionary *categoryDictionary;
@property (nonatomic) int category;

- (IBAction)changeSign:(id)sender;
- (void)unloadPage:(int)page;
- (void)loadPage:(int)page;

@end