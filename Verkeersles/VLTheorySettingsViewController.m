//
//  VLTheorySettingsViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 1/3/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import "VLTheorySettingsViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLTheorySettingsViewController(Private)

- (void)saveSettings;
- (void)releaseIBOutlets;

@end

@implementation VLTheorySettingsViewController

@synthesize lblShowImagesWithText, showImagesWithText, lblNumberOfSecondsToShowQuestion, lblNumberOfSecondsToAnswerTheQuestion, lblNumberOfSecondsToShowAnswer, lblNumberOfSecondsToShowError, numberOfSecondsToShowQuestion, numberOfSecondsToAnswerTheQuestion, numberOfSecondsToShowAnswer, numberOfSecondsToShowError, lblSeconds1, lblSeconds2, lblSeconds3, lblSeconds4;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [lblShowImagesWithText setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblShowImagesWithText.text = NSLocalizedString(@"ShowImagesWithText", @"ShowImagesWithText");
    if ([[defaults stringForKey:@"showImagesWithText"] isEqualToString:@"YES"]) {
        showImagesWithText.on = YES;
    }
    else {
        showImagesWithText.on = NO;
    }

    [lblNumberOfSecondsToShowQuestion setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblNumberOfSecondsToShowQuestion.text = NSLocalizedString(@"NumberOfSecondsToShowQuestion", @"NumberOfSecondsToShowQuestion");
    numberOfSecondsToShowQuestion.text = [defaults stringForKey:@"numberOfSecondsToShowQuestion"];
    
    [lblNumberOfSecondsToAnswerTheQuestion setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblNumberOfSecondsToAnswerTheQuestion.text = NSLocalizedString(@"NumberOfSecondsToAnswerTheQuestion", @"NumberOfSecondsToAnswerTheQuestion");
    numberOfSecondsToAnswerTheQuestion.text = [defaults stringForKey:@"numberOfSecondsToAnswerTheQuestion"];
    
    [lblNumberOfSecondsToShowAnswer setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblNumberOfSecondsToShowAnswer.text = NSLocalizedString(@"NumberOfSecondsToShowAnswer", @"NumberOfSecondsToShowAnswer");
    numberOfSecondsToShowAnswer.text = [defaults stringForKey:@"numberOfSecondsToShowAnswer"];

    [lblNumberOfSecondsToShowError setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblNumberOfSecondsToShowError.text = NSLocalizedString(@"NumberOfSecondsToShowError", @"NumberOfSecondsToShowError");
    numberOfSecondsToShowError.text = [defaults stringForKey:@"numberOfSecondsToShowError"];
    
    [lblSeconds1 setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblSeconds1.text = NSLocalizedString(@"Seconden", @"Seconden");
    
    [lblSeconds2 setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblSeconds2.text = NSLocalizedString(@"Seconden", @"Seconden");
    
    [lblSeconds3 setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblSeconds3.text = NSLocalizedString(@"Seconden", @"Seconden");
    
    [lblSeconds4 setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    lblSeconds4.text = NSLocalizedString(@"Seconden", @"Seconden");
    
    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficQuizSettingsTitle", @"TrafficQuizSettingsTitle")];
    
    [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Bewaar", @"Bewaar")];
    
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self saveSettings];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self.tableView reloadData];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficQuizSettingsTitle", @"TrafficQuizSettingsTitle")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
	[self releaseIBOutlets];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle;
    
    switch (section)
    {
        case 0:
            sectionTitle = NSLocalizedString(@"TrafficQuizSettingsPresentationTitle", @"TrafficQuizSettingsPresentationTitle");
            
            break;
             
        case 1:
            sectionTitle = NSLocalizedString(@"TrafficQuizSettingsTimersTitle", @"TrafficQuizSettingsTimersTitle");
            
            break;
            
        default:
            sectionTitle = @"";
            
            break;
    }
    
    return sectionTitle;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *) view;
    [header.contentView setBackgroundColor:TABLEVIEW_SECTION_COLOR];
    [header.contentView setAlpha:0.6];
    [header.textLabel setTextColor:TABLEVIEW_SECTION_TEXTCOLOR];
    [header.textLabel setOpaque:NO];
    [header.textLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *) view;
    [footer.contentView setBackgroundColor:TABLEVIEW_SECTION_COLOR];
    [footer.contentView setAlpha:0.6];
    [footer.contentView setFrame:CGRectMake(footer.contentView.frame.origin.x, footer.contentView.frame.origin.y, footer.contentView.frame.size.width, 0)];
}

@end

@implementation VLTheorySettingsViewController(Private)

#pragma mark -
#pragma mark Settings

- (void)saveSettings {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (showImagesWithText.on) {
        [defaults setObject:@"YES" forKey:@"showImagesWithText"];
    }
    else {
        [defaults setObject:@"NO" forKey:@"showImagesWithText"];
    }

	if([[numberOfSecondsToShowQuestion text] intValue] > 0){
		[defaults setObject:[numberOfSecondsToShowQuestion text] forKey:@"numberOfSecondsToShowQuestion"];
	}
	else {
		numberOfSecondsToShowQuestion.text = [defaults stringForKey:@"numberOfSecondsToShowQuestion"];
	}
	
	if([[numberOfSecondsToAnswerTheQuestion text] intValue] > 0){
		[defaults setObject:[numberOfSecondsToAnswerTheQuestion text] forKey:@"numberOfSecondsToAnswerTheQuestion"];
	}
	else {
		numberOfSecondsToAnswerTheQuestion.text = [defaults stringForKey:@"numberOfSecondsToAnswerTheQuestion"];
	}
	
	if([[numberOfSecondsToShowAnswer text] intValue] > 0){
		[defaults setObject:[numberOfSecondsToShowAnswer text] forKey:@"numberOfSecondsToShowAnswer"];
	}
	else {
		numberOfSecondsToShowAnswer.text = [defaults stringForKey:@"numberOfSecondsToShowAnswer"];
	}
	
	if([[numberOfSecondsToShowError text] intValue] > 0){
		[defaults setObject:[numberOfSecondsToShowError text] forKey:@"numberOfSecondsToShowError"];
	}
	else {
		numberOfSecondsToShowError.text = [defaults stringForKey:@"numberOfSecondsToShowError"];
	}
	
	[numberOfSecondsToShowQuestion resignFirstResponder];
	[numberOfSecondsToAnswerTheQuestion resignFirstResponder];
	[numberOfSecondsToShowAnswer resignFirstResponder];
	[numberOfSecondsToShowError resignFirstResponder];
}

- (void)releaseIBOutlets {
    self.showImagesWithText = nil;
    self.lblShowImagesWithText = nil;

    self.lblNumberOfSecondsToShowQuestion = nil;
    self.lblNumberOfSecondsToAnswerTheQuestion = nil;
    self.lblNumberOfSecondsToShowAnswer = nil;
    self.lblNumberOfSecondsToShowError = nil;
    
    self.lblSeconds1 = nil;
    self.lblSeconds2 = nil;
    self.lblSeconds3 = nil;
    self.lblSeconds4 = nil;
    
	self.numberOfSecondsToShowQuestion = nil;
	self.numberOfSecondsToAnswerTheQuestion = nil;
	self.numberOfSecondsToShowAnswer = nil;
	self.numberOfSecondsToShowError = nil;
}

@end
