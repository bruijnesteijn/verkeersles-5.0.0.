//
//  VLListOfTermsTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfTermsTableViewController : UITableViewController {
    NSDictionary *dataSource;
    NSArray *keys;
}

@property (nonatomic, retain) NSDictionary *dataSource;
@property (nonatomic, retain) NSArray *keys;

@end
