//
//  VLListOfTrafficSignsPageContentViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfTrafficSignsPageContentViewController : UIViewController {
    NSUInteger currentPageNumber;
    NSUInteger numberOfPages;
    
    IBOutlet UIImageView *signImage;
    NSString *signImageName;
    
    IBOutlet UILabel* category;
    NSString *categoryLabelText;
    
    IBOutlet UILabel* sign;
    NSString *signLabelText;
    
    IBOutlet UIButton* page;
    NSString *pageLabelText;
}

@property IBOutlet UIPageControl *pageControl;
@property NSUInteger currentPageNumber;
@property NSUInteger numberOfPages;

@property (nonatomic, retain) IBOutlet UIImageView *signImage;
@property NSString *signImageName;

@property (nonatomic, retain) IBOutlet UILabel* category;
@property NSString *categoryLabelText;

@property (nonatomic, retain) IBOutlet UILabel* sign;
@property NSString *signLabelText;

@property (nonatomic, retain) IBOutlet UIButton* page;
@property NSString *pageLabelText;

@end
