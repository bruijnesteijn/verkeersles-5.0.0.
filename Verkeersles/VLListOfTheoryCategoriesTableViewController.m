//
//  VLListOfTheoryCategoriesTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/03/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLListOfTheoryCategoriesTableViewController.h"
#import "VLListOfTheoryCategoriesTableViewCell.h"
#import "VLListOfItemsForCategoryTableViewController.h"
#import "VLAppDelegate.h"
#import "VLTheoryCategory.h"
#import "VLTheoryItem.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfTheoryCategoriesTableViewController(private)

- (void)addTheoryItemsFromFileIntoDatabase;
- (void)queryDB;

@end

@implementation VLListOfTheoryCategoriesTableViewController

@synthesize fetchedResultsController = _fetchedResultsController, searchController, searchAllCategoriesPredicate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self addTheoryItemsFromFileIntoDatabase];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.delegate = self;
    self.searchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self queryDB];
 
    [self.searchController.searchBar setPlaceholder:NSLocalizedString(@"SearchInAllCategories", @"SearchInAllCategories")];
    self.searchAllCategoriesPredicate = nil;

    [[self navigationItem] setTitle:NSLocalizedString(@"TheoryTitle", @"TheoryTitle")];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self queryDB];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"TheoryTitle", @"TheoryTitle")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc{
    [self.searchController.view removeFromSuperview];
}

#pragma mark - UISearchControllerDelegate

- (void)willPresentSearchController:(UISearchController *)theSearchController {
    [self.tableView setUserInteractionEnabled:NO];
    
    [self.searchController.searchBar setPlaceholder:NSLocalizedString(@"SearchInAllCategories", @"SearchInAllCategories")];
    self.searchAllCategoriesPredicate = @"";
    self.searchController.searchBar.text = @"";
}

- (void)didPresentSearchController:(UISearchController *)theSearchController {
}

- (void)willDismissSearchController:(UISearchController *)theSearchController {
}

- (void)didDismissSearchController:(UISearchController *)theSearchController {
    [self.tableView setUserInteractionEnabled:YES];
    
    [self.searchController.searchBar setPlaceholder:NSLocalizedString(@"SearchInAllCategories", @"SearchInAllCategories")];
    self.searchAllCategoriesPredicate = nil;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.tableView setUserInteractionEnabled:YES];
    
    [self.searchController.searchBar setPlaceholder:NSLocalizedString(@"SearchInAllCategories", @"SearchInAllCategories")];
    self.searchAllCategoriesPredicate = nil;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.tableView setUserInteractionEnabled:YES];
    
    [self.searchController setActive:NO];
    
    [self performSegueWithIdentifier:@"showListOfItemsForCategory" sender:self];
}

#pragma mark - UISearchResultsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)theSearchController {
    NSString *searchText = theSearchController.searchBar.text;
    if ([searchText length] != 0) {
        NSString *lastChar = [searchText substringFromIndex:searchText.length - 1];
        self.searchAllCategoriesPredicate = [self.searchAllCategoriesPredicate stringByAppendingString:lastChar];
    }
}

#pragma mark - Table view data source

- (VLListOfTheoryCategoriesTableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLTheoryCategoryCell";

    VLListOfTheoryCategoriesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(VLListOfTheoryCategoriesTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    VLTheoryCategory *oneCategory = (VLTheoryCategory *) [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self setTitleForCell:cell labelText:[oneCategory valueForKey:NSLocalizedString(@"TheoryCategoryName", @"TheoryCategoryName")]];
    [self setSubtitleForCell:cell labelText:[oneCategory valueForKey:NSLocalizedString(@"TheoryCategoryDescription", @"TheoryCategoryDescription")]];
    [self setImageForCell:cell imageName:[oneCategory valueForKey:NSLocalizedString(@"TheoryCategoryIcon", @"TheoryCategoryIcon")]];
}

- (void)setTitleForCell:(VLListOfTheoryCategoriesTableViewCell *)cell labelText:(NSString *)text {
    [cell.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [cell.titleLabel setNumberOfLines:0];
    [cell.titleLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.titleLabel setText:text];
}

- (void)setSubtitleForCell:(VLListOfTheoryCategoriesTableViewCell *)cell labelText:(NSString *)text {
    [cell.subtitleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.subtitleLabel setNumberOfLines:0];
    [cell.subtitleLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.subtitleLabel setText:text];
}

- (void)setImageForCell:(VLListOfTheoryCategoriesTableViewCell *)cell imageName:(NSString *)text {
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@", text]];
    [cell.image setImage:img];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellAtIndexPath:indexPath];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSUInteger count = [[self.fetchedResultsController sections] count];
    if (count == 0) {
        count = 1;
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [self.fetchedResultsController sections];
    NSUInteger count = 0;
    if ([sections count]) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        count = [sectionInfo numberOfObjects];
    }
    
    return count;
}

#pragma mark -
#pragma mark Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showListOfItemsForCategory"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        VLListOfItemsForCategoryTableViewController *listOfItemsForCategory = segue.destinationViewController;

        if(self.searchAllCategoriesPredicate != nil  && ![self.searchAllCategoriesPredicate isEqualToString:@""]) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            listOfItemsForCategory.category = (VLTheoryCategory *)[self.fetchedResultsController objectAtIndexPath:indexPath];
            listOfItemsForCategory.searchAllCategoriesPredicate = self.searchAllCategoriesPredicate;
            listOfItemsForCategory.searchOneCategoryPredicate = nil;
            listOfItemsForCategory.searchAllCategories = YES;
        }
        else {
            listOfItemsForCategory.category = (VLTheoryCategory *)[self.fetchedResultsController objectAtIndexPath:indexPath];
            listOfItemsForCategory.searchAllCategoriesPredicate = @"";
            listOfItemsForCategory.searchOneCategoryPredicate = nil;
            listOfItemsForCategory.searchAllCategories = NO;
        }
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
}

#pragma mark -
#pragma mark Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    VLAppDelegate *appDelegate = (VLAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"VLTheoryCategory" inManagedObjectContext:managedObjectContext];
    
    NSString *sectionKey = nil;
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"theoryCategoryID" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    sectionKey = NSLocalizedString(@"theoryCategoryName", @"theoryCategoryName");
   	[fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:managedObjectContext
                                                                            sectionNameKeyPath:sectionKey
                                                                                     cacheName:@"theoryCategory"];
    frc.delegate = self;
    _fetchedResultsController = frc;
    
    return _fetchedResultsController;
}

@end

@implementation VLListOfTheoryCategoriesTableViewController (Private)

- (void)addTheoryItemsFromFileIntoDatabase {
    NSError * error = nil;
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    if (![[defaults stringForKey:@"coreDataVersion"] isEqualToString:version]) {
        NSFetchRequest * allTheoryItems = [[NSFetchRequest alloc] init];
        [allTheoryItems setEntity:[NSEntityDescription entityForName:@"VLTheoryCategory" inManagedObjectContext:context]];
        [allTheoryItems setIncludesPropertyValues:NO];
        
        NSArray * items = [context executeFetchRequest:allTheoryItems error:&error];
        for (NSManagedObject * item in items) {
            [context deleteObject:item];
        }
        
        [self.fetchedResultsController performFetch:&error];
        
        [defaults setObject:version forKey:@"coreDataVersion"];
    
        if ([[self.fetchedResultsController fetchedObjects] count] == 0) {
            [self.tableView reloadData];
        
            [self importPlistToDatabase:context entity:entity NSString:@"A" NSString:@"VLTheoryPartA.plist"];
            [self importPlistToDatabase:context entity:entity NSString:@"B" NSString:@"VLTheoryPartB.plist"];
            [self importPlistToDatabase:context entity:entity NSString:@"C" NSString:@"VLTheoryPartC.plist"];
            [self importPlistToDatabase:context entity:entity NSString:@"D" NSString:@"VLTheoryPartD.plist"];
            [self importPlistToDatabase:context entity:entity NSString:@"E" NSString:@"VLTheoryPartE.plist"];
            [self importPlistToDatabase:context entity:entity NSString:@"F" NSString:@"VLTheoryPartF.plist"];
        
            if (![context save:&error]) {
                NSLog(NSLocalizedString(@"ErrorSavingDatabaseMessage", @"ErrorSavingDatabaseMessage"), [error localizedDescription]);
            }
        }
    }
}

- (void)importPlistToDatabase:(NSManagedObjectContext *)context entity:(NSEntityDescription *)entity NSString:categoryID NSString:plistFilename  {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = [path stringByAppendingPathComponent:plistFilename];
    NSDictionary *ds = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    VLTheoryCategory  *category = (VLTheoryCategory *) [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
    [category setTheoryCategoryID:categoryID];
    [category setTheoryCategoryNaam:[ds valueForKey:@"theoryCategoryNaam"]];
    [category setTheoryCategoryBeschrijving:[ds valueForKey:@"theoryCategoryBeschrijving"]];
    [category setTheoryCategoryName:[ds valueForKey:@"theoryCategoryName"]];
    [category setTheoryCategoryDescription:[ds valueForKey:@"theoryCategoryDescription"]];
    [category setTheoryCategoryIcon:[ds valueForKey:@"theoryCategoryIcon"]];
    
    NSArray *itemsIn = [ds objectForKey:@"items"];
    
    NSMutableArray *itemsOut = [[NSMutableArray alloc] init];
    VLTheoryItem *newItem = nil;

    for (int i = 0; i < [itemsIn count]; i++) {
        newItem = (VLTheoryItem *) [NSEntityDescription insertNewObjectForEntityForName:@"VLTheoryItem" inManagedObjectContext:context];
        [newItem setTheoryItemID:[NSString stringWithFormat:@"%d", i]];
        [newItem setTheoryItemVraag:[[itemsIn objectAtIndex:i] valueForKey:@"theoryItemVraag"]];
        [newItem setTheoryItemAntwoord:[[itemsIn objectAtIndex:i] valueForKey:@"theoryItemAntwoord"]];
        [newItem setTheoryItemQuestion:[[itemsIn objectAtIndex:i] valueForKey:@"theoryItemQuestion"]];
        [newItem setTheoryItemAnswer:[[itemsIn objectAtIndex:i] valueForKey:@"theoryItemAnswer"]];
        [newItem setBelongsToVLTheoryCategory:category];
        
        [itemsOut addObject:newItem];
    }
    
    NSSet *items = [[NSSet alloc] initWithArray:itemsOut];
    
    [category setHasVLTheoryItem:items];
}

- (void)queryDB {
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ErrorLoadingDatabaseTitle", @"ErrorLoadingDatabaseTitle") message:[NSString stringWithFormat:NSLocalizedString(@"ErrorLoadingDatabaseMessage", @"ErrorLoadingDatabaseMessage"), [error localizedDescription]] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *continueWithoutAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }];
        
        [alert addAction:continueWithoutAction];
    }
    
    [self.tableView reloadData];
}

@end
