//
//  VLLanguageTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLLanguageTableViewController : UITableViewController {
    NSArray *dataSource;
}

@property (nonatomic, retain) NSArray *dataSource;

@end