//
//  VLListOfLawSectionsTableViewCell.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/22/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfLawSectionsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *image;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;

@end
