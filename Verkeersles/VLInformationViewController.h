//
//  VLInformationViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLInformationViewController : UIViewController

@property (nonatomic, retain) NSDictionary *dataSource;

@property (strong, nonatomic) IBOutlet UIScrollView *informationScrollView;
@property (strong, nonatomic) IBOutlet UILabel *informationName;
@property (strong, nonatomic) IBOutlet UILabel *informationVersion;
@property (strong, nonatomic) IBOutlet UILabel *informationCopyright;
@property (strong, nonatomic) IBOutlet UILabel *informationBody;

@end
