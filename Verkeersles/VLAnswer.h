//
//  VLAnswer.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 15/12/15.
//  Copyright © 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLAnswer : NSObject {
    NSString *answerExamID;
    NSString *answerID;
    NSString *answerCorrectOption;
    NSString *answerSelectedOption;
    NSString *answerCorrect;
}

@property (nonatomic, retain) NSString *answerExamID;
@property (nonatomic, retain) NSString *answerID;
@property (nonatomic, retain) NSString *answerCorrectOption;
@property (nonatomic, retain) NSString *answerSelectedOption;
@property (nonatomic, retain) NSString *answerCorrect;

- (instancetype)init;

@end
