//
//  VLListOfArticlesForSectionTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLListOfArticlesForSectionTableViewController.h"
#import "VLListOfArticlesForSectionTableViewCell.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfArticlesForSectionTableViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLListOfArticlesForSectionTableViewController

@synthesize dataSource, section;

#pragma mark -
#pragma mark View lifecycle

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];
    
    [[self navigationItem] setTitle:[self.dataSource objectForKey:@"categorie"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];

    [[self navigationItem] setTitle:[self.dataSource objectForKey:@"categorie"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Table view data source

- (VLListOfArticlesForSectionTableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLLawArticleCell";
    
    VLListOfArticlesForSectionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(VLListOfArticlesForSectionTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *text = [[self.dataSource objectForKey:@"artikelen"] objectAtIndex:indexPath.row];
    
    [self setTitleForCell:cell labelText:[NSString stringWithFormat:@"%@", text]];
    [self setSubtitleForCell:cell labelText:[NSString stringWithFormat:@"%@", text]];
}

- (void)setTitleForCell:(VLListOfArticlesForSectionTableViewCell *)cell labelText:(NSString *)text {
    NSRange range = [text rangeOfString:@" "];
    
    [cell.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [cell.titleLabel setNumberOfLines:0];
    [cell.titleLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.titleLabel setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Article", @"Article"), [text substringToIndex:range.location+1]]];
}

- (void)setSubtitleForCell:(VLListOfArticlesForSectionTableViewCell *)cell labelText:(NSString *)text {
    NSRange range = [text rangeOfString:@" "];
    
    [cell.subtitleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.subtitleLabel setNumberOfLines:0];
    [cell.subtitleLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.subtitleLabel setText:[NSString stringWithFormat:@"%@", [text substringFromIndex:range.location+1]]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellAtIndexPath:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.dataSource objectForKey:@"artikelen"] count];
}

@end

@implementation VLListOfArticlesForSectionTableViewController(Private)

- (void)initializeDataSource {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficLaws_datasource", @"VLTrafficLaws_datasource")];
    
    self.dataSource = [[[NSDictionary dictionaryWithContentsOfFile:plistPath] objectForKey:@"regels"] objectAtIndex:section];

    [self.tableView reloadData];
}

@end


