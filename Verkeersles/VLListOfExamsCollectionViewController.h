//
//  VLListOfExamsCollectionViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 11/11/15.
//  Copyright © 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfExamsCollectionViewController : UICollectionViewController

@end
