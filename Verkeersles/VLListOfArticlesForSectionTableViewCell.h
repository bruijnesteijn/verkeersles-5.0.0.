//
//  VLListOfArticlesForSectionTableViewCell.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/22/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfArticlesForSectionTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;

@end
