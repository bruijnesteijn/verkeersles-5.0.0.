//
//  VLMenuViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLMenuViewController.h"
#import "VLLocalizationUtility.h"
#import "VLConstants.h"

@interface VLMenuViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLMenuViewController

@synthesize menuOption1, menuOption2, menuOption3, menuOption4, menuOption5, menuOption6;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Menu", @"Menu")];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredContentSizeChanged:)
                                                 name: UIContentSizeCategoryDidChangeNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)preferredContentSizeChanged:(NSNotification *) notification
{
    [self initializeDataSource];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Menu", @"Menu")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

@implementation VLMenuViewController(Private)

- (void)initializeDataSource {
    [self.menuOption1.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.menuOption1.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [self.menuOption2.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.menuOption2.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [self.menuOption3.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.menuOption3.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [self.menuOption4.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.menuOption4.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [self.menuOption5.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.menuOption5.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [self.menuOption6.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.menuOption6.titleLabel setTextAlignment: NSTextAlignmentCenter];

    [self.menuOption1 setTitle:NSLocalizedString(@"TrafficSignsTitle", @"TrafficSignsTitle") forState:UIControlStateNormal];
    [self.menuOption2 setTitle:NSLocalizedString(@"TheoryTitle", @"TheoryTitle") forState:UIControlStateNormal];
    [self.menuOption3 setTitle:NSLocalizedString(@"TrafficLawTitle", @"TrafficLawTitle") forState:UIControlStateNormal];
    [self.menuOption4 setTitle:NSLocalizedString(@"ExamsTitle", @"ExamsTitle") forState:UIControlStateNormal];
    [self.menuOption5 setTitle:NSLocalizedString(@"TerminologyTitle", @"TerminologyTitle") forState:UIControlStateNormal];
    [self.menuOption6 setTitle:NSLocalizedString(@"SettingsTitle", @"SettingsTitle") forState:UIControlStateNormal];
}

@end
