//
//  VLConstants.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

//#define default for application
#define APP_BLUE_COLOR [UIColor colorWithRed:0.0/255.0 green:64.0/255.0 blue:128.0/255.0 alpha:1.0]
#define APP_UITABLEVIEW_REGULAR_ESTIMATEDROWHEIGHT 44

#define APP_NUMBER_OF_EXAMS 12
#define APP_EXAM_ANSWERS_FILENAME_VERSION 400
#define APP_EXAM_NUMBER_OF_QUESTIONS 30

#define APP_NAVIGATION_BAR_TINT [UIColor colorWithRed:96.0/255.0 green:96.0/255.0 blue:96.0/255.0 alpha:1.0]

// Page controller definitions & constants
#define PAGECONTROLLER_COLOR [UIColor colorWithRed:0.0/255.0 green:125.0/255.0 blue:198.0/255.0 alpha:1.0]

// TableView definitions & constants
#define TABLEVIEW_SECTION_COLOR [UIColor colorWithRed:0.0/255.0 green:125.0/255.0 blue:198.0/255.0 alpha:1.0]
#define TABLEVIEW_SECTION_HEIGHT 44
#define TABLEVIEW_SECTION_TEXTCOLOR [UIColor whiteColor]
#define TABLEVIEW_SECTION_LIGHT_COLOR [UIColor colorWithRed:0.0/255.0 green:160.0/255.0 blue:220.0/255.0 alpha:1.0]
#define TABLEVIEW_SECTION_DARK_COLOR [UIColor colorWithRed:0.0/255.0 green:125.0/255.0 blue:198.0/255.0 alpha:1.0]
#define TABLEVIEW_ROW_TITLE_COLOR [UIColor colorWithRed:0.0/255.0 green:64.0/255.0 blue:128.0/255.0 alpha:1.0]
#define TABLEVIEW_ROW_SUBTITLE_COLOR [UIColor colorWithRed:48.0/255.0 green:48.0/255.0 blue:48.0/255.0 alpha:1.0]
#define TABLEVIEW_ROW_BODY_TEXTCOLOR [UIColor colorWithRed:96.0/255.0 green:96.0/255.0 blue:96.0/255.0 alpha:1.0]
#define TABLEVIEW_ROW_RESET_BACKGROUNDCOLOR [UIColor colorWithRed:212.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]

// Sign characteristics
#define SIGN_SELECTION_BLUE_COLOR [UIColor colorWithRed:40.0/255.0 green:85.0/255.0 blue:103.0/255.0 alpha:1.0]
#define SIGN_SELECTION_RED_COLOR [UIColor colorWithRed:255.0/255.0 green:38.0/255.0 blue:0.0/255.0 alpha:1.0]
#define SIGN_SELECTION_YELLOW_COLOR [UIColor colorWithRed:230.0/255.0 green:180.0/255.0 blue:45.0/255.0 alpha:1.0]
#define SIGN_SELECTION_GREEN_COLOR [UIColor colorWithRed:0.0/255.0 green:130.0/255.0 blue:0.0/255.0 alpha:1.0]
#define SIGN_SELECTION_LIGHTGRAY_COLOR [UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1.0]
#define SIGN_SELECTION_GRAY_COLOR [UIColor colorWithRed:96.0/255.0 green:96.0/255.0 blue:96.0/255.0 alpha:1.0]

#define SIGN_SHAPE_CIRCLE 1
#define SIGN_SHAPE_SQUARE 2
#define SIGN_SHAPE_RECTANGLE_LANDSCAPE 3
#define SIGN_SHAPE_RECTANGLE_PORTRAIT 4
#define SIGN_SHAPE_TRIANGLE_BOTTOM 5
#define SIGN_SHAPE_TRIANGLE_TOP 6

#define SIGN_COLOR_BLUE 7
#define SIGN_COLOR_RED 8
#define SIGN_COLOR_YELLOW 9
#define SIGN_COLOR_GREEN 10

#define SIGN_TEXT_NUMBERS 11
#define SIGN_TEXT_LETTERS 12
#define SIGN_TEXT_WORDS 13

#define SIGN_IMAGE_CAR 14
#define SIGN_IMAGE_TRUCK 15
#define SIGN_IMAGE_MOTOR 16
#define SIGN_IMAGE_SCOOTER 17
#define SIGN_IMAGE_BICYCLE 18
#define SIGN_IMAGE_PERSON 19
#define SIGN_IMAGE_BUS 20
#define SIGN_IMAGE_TRAIN 21
#define SIGN_IMAGE_TRACTOR 22
#define SIGN_IMAGE_ANIMAL 23
#define SIGN_IMAGE_ARROW 24
#define SIGN_IMAGE_OTHER 25

// Exam colors
#define DEFAULT_COLOR [UIColor colorWithRed:48.0/255.0 green:48.0/255.0 blue:48.0/255.0 alpha:1.0]
#define QUESTION_COLOR [UIColor colorWithRed:0.0/255.0 green:64.0/255.0 blue:128.0/255.0 alpha:1.0]
#define ANSWER_COLOR [UIColor colorWithRed:0.0/255.0 green:64.0/255.0 blue:128.0/255.0 alpha:1.0]
#define SELECTED_COLOR [UIColor colorWithRed:0.0/255.0 green:64.0/255.0 blue:128.0/255.0 alpha:1.0]
#define CORRECT_COLOR [UIColor colorWithRed:0.0/255.0 green:130.0/255.0 blue:0.0/255.0 alpha:1.0]
#define FALSE_COLOR [UIColor colorWithRed:255.0/255.0 green:38.0/255.0 blue:0.0/255.0 alpha:1.0]
#define PROGRESSBAR_RED_COLOR [UIColor colorWithRed:255.0/255.0 green:38.0/255.0 blue:0.0/255.0 alpha:1.0]


