//
//  VLListOfTrafficSignsTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/20/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfTrafficSignsTableViewController : UITableViewController {
    NSDictionary *dataSource;
    int category;
    
    UITraitCollection *previous;
    UITraitCollection *current;
}

@property (nonatomic, retain) NSDictionary *dataSource;
@property (nonatomic) int category;

@property (nonatomic, retain) UITraitCollection *previous;
@property (nonatomic, retain) UITraitCollection *current;

@end
