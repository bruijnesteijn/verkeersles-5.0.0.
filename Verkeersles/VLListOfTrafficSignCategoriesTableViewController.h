//
//  VLListOfTrafficSignCategoriesTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/19/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfTrafficSignCategoriesTableViewController : UITableViewController {
    NSArray *dataSource;
}

@property (nonatomic, retain) NSArray *dataSource;

@end
