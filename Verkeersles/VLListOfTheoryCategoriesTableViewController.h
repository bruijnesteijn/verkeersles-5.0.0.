//
//  VLListOfTheoryCategoriesTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/03/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface VLListOfTheoryCategoriesTableViewController:UITableViewController <NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate> {
    NSFetchedResultsController *_fetchedResultsController;
    
    UISearchController *searchController;
    NSString *searchAllCategoriesPredicate;
}

@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, retain) NSString *searchAllCategoriesPredicate;

@end
