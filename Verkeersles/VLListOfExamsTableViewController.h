//
//  VLListOfExamsTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/19/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLExamsSelectionViewController.h"
#import "VLListOfExamsTableViewCell.h"

@interface VLListOfExamsTableViewController : UITableViewController <UIPopoverPresentationControllerDelegate, UIAlertViewDelegate> {
    VLExamsSelectionViewController *popoverPresentationController;
    UIPopoverPresentationController * popoverController;

    NSIndexPath *selectedCellIndexPath;
    VLListOfExamsTableViewCell *selectedCell;
    CGRect selectedCellRect;
    
    BOOL isShowingPopover;
    BOOL isShowingNextViewController;
    
    NSString *documentsPathsForSelectedExam;
    NSDictionary *answersForSelectedExam;
    int numberOfQuestionsAnsweredForSelectedExam;
    int totalNumberOfQuestionsForSelectedExam;
    int numberOfQuestionsAnsweredIncorrectForSelectedExam;
    
    NSString *typeOfExam;
    int numberOfExam;
}

@property (nonatomic, retain) VLExamsSelectionViewController *popoverPresentationController;
@property (nonatomic, retain) UIPopoverPresentationController * popoverController;

@property (nonatomic, retain) VLListOfExamsTableViewCell *selectedCell;
@property CGRect selectedCellRect;

@property (nonatomic, retain)  NSString *documentsPathForSelectedExam;
@property (nonatomic, retain) NSDictionary *answersForSelectedExam;
@property int numberOfQuestionsAnsweredForSelectedExam;
@property int totalNumberOfQuestionsForSelectedExam;
@property int numberOfQuestionsAnsweredIncorrectForSelectedExam;

@property (nonatomic, retain) NSString *typeOfExam;
@property int numberOfExam;

@end
