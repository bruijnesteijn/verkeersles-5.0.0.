//
//  VLExamsSelectionViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 11/11/15.
//  Copyright © 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLExamsSelectionViewController.h"
#import "VLListOfExamsTableViewController.h"
#import "VLQuestionsForExamViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLExamsSelectionViewController(Private) 

- (void)initializeDataSource;

- (IBAction)firstViewTapped;
- (IBAction)secondViewTapped;
- (IBAction)thirdViewTapped;

@end


@implementation VLExamsSelectionViewController

@synthesize examHasErrors, examCompleted, firstStackView, firstImage, firstTitleLabel, secondStackView, secondTitleLabel, secondImage, thirdStackView, thirdImage, thirdTitleLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *firstTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(firstViewTapped)];
    firstTapRecognizer.numberOfTapsRequired = 1;
    [firstStackView addGestureRecognizer:firstTapRecognizer];

    UITapGestureRecognizer *secondTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(secondViewTapped)];
    secondTapRecognizer.numberOfTapsRequired = 1;
    [secondStackView addGestureRecognizer:secondTapRecognizer];
    
    UITapGestureRecognizer *thirdTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(thirdViewTapped)];
    thirdTapRecognizer.numberOfTapsRequired = 1;
    [thirdStackView addGestureRecognizer:thirdTapRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self initializeDataSource];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

@implementation VLExamsSelectionViewController(Private)

- (void)initializeDataSource {
    self.firstTitleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    self.secondTitleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    self.thirdTitleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"None" forKey:@"typeOfExam"];
    
    self.firstTitleLabel.text = NSLocalizedString(@"Practise", @"Practise");
    [self.firstImage setImage:[UIImage imageNamed:@"lesson"]];
    
    self.secondTitleLabel.text = NSLocalizedString(@"Exam", @"Exam");
    [self.secondImage setImage:[UIImage imageNamed:@"g3"]];
    
    self.thirdTitleLabel.text = NSLocalizedString(@"Errors", @"Errors");
    [self.thirdImage setImage:[UIImage imageNamed:@"j9"]];
    
    if(!self.examHasErrors) {
        [self.thirdStackView setHidden:TRUE];
    }
    if(self.examCompleted) {
        [self.secondStackView setHidden:TRUE];
    }
}

- (void)firstViewTapped {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"Practise" forKey:@"typeOfExam"];
    
    [self dismissViewControllerAnimated:YES completion:^{[self returnedFromPopover];}];
}

- (void)secondViewTapped {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"Exam" forKey:@"typeOfExam"];
    
    [self dismissViewControllerAnimated:YES completion:^{[self returnedFromPopover];}];
}

- (void)thirdViewTapped {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"Errors" forKey:@"typeOfExam"];
    
    [self dismissViewControllerAnimated:YES completion:^{[self returnedFromPopover];}];
}

-(void)returnedFromPopover{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PopoverDismissedNotification" object:self];
}


@end