//
//  VLInformationViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 10/12/13.
//  Copyright (c) 2013 Bart Bruijnesteijn. All rights reserved.
//

#import "VLInformationViewController.h"
#import "VLLocalizationUtility.h"
#import "VLConstants.h"

@interface VLInformationViewController(Private)

- (void)initializeDataSource;

@end

@implementation VLInformationViewController

@synthesize informationScrollView, dataSource, informationName, informationVersion, informationCopyright, informationBody;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];

    [[self navigationItem] setTitle:NSLocalizedString(@"Informatie", @"Informatie")];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    dataSource = nil;
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Informatie", @"Informatie")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

@implementation VLInformationViewController(Private)

- (void)initializeDataSource {
    [self.informationName setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [self.informationVersion setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [self.informationCopyright setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [self.informationBody setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]];

    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *plistPath = nil;
    
    plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLInformation_datasource", @"VLInformation_datasource")];
    
    self.dataSource = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    [self.informationName setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [self.informationName setMinimumScaleFactor:0.5];
    [self.informationName setTextAlignment:NSTextAlignmentCenter];
    [self.informationName setLineBreakMode: NSLineBreakByWordWrapping];
    [self.informationName setNumberOfLines:0];
    [self.informationName setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [self.informationName setText:[self.dataSource valueForKey:@"informationName"]];
    
    [self.informationVersion setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [self.informationVersion setMinimumScaleFactor:0.5];
    [self.informationVersion setTextAlignment:NSTextAlignmentCenter];
    [self.informationVersion setLineBreakMode: NSLineBreakByWordWrapping];
    [self.informationVersion setNumberOfLines:0];
    [self.informationVersion setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [self.informationVersion setText:[self.dataSource valueForKey:@"informationVersion"]];
    
    [self.informationCopyright setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [self.informationCopyright setMinimumScaleFactor:0.5];
    [self.informationCopyright setTextAlignment:NSTextAlignmentCenter];
    [self.informationCopyright setLineBreakMode: NSLineBreakByWordWrapping];
    [self.informationCopyright setNumberOfLines:0];
    [self.informationCopyright setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [self.informationCopyright setText:[self.dataSource valueForKey:@"informationCopyright"]];
    
    [self.informationBody setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]];
    [self.informationBody setMinimumScaleFactor:0.5];
    [self.informationBody setTextAlignment:NSTextAlignmentLeft];
    [self.informationBody setLineBreakMode: NSLineBreakByWordWrapping];
    [self.informationBody setNumberOfLines:0];
    [self.informationBody setTextColor:TABLEVIEW_ROW_BODY_TEXTCOLOR];
    [self.informationBody setText:[self.dataSource valueForKey:@"informationBody"]];
}

@end
