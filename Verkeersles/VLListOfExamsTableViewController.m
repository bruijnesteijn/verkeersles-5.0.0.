//
//  VLListOfExamsTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/19/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import "VLListOfExamsTableViewController.h"
#import "VLListOfExamsTableViewCell.h"
#import "VLQuestionsForExamViewController.h"
#import "VLAppDelegate.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLListOfExamsTableViewController(Private)

- (void)initializeAnswersForSelectedExam;
- (void)proceedToQuestionsForExamViewController;

@end

@implementation VLListOfExamsTableViewController

@synthesize popoverPresentationController, popoverController, selectedCell, selectedCellRect, documentsPathForSelectedExam,answersForSelectedExam, numberOfQuestionsAnsweredForSelectedExam, numberOfQuestionsAnsweredIncorrectForSelectedExam, totalNumberOfQuestionsForSelectedExam, typeOfExam, numberOfExam;

#pragma mark -
#pragma mark View lifecycle

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Examens", @"Examens")];
    
    isShowingPopover = FALSE;
    isShowingNextViewController = FALSE;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popoverPresentationControllerDidDismissPopover:)
                                                 name: @"PopoverDismissedNotification"
                                               object: nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self.tableView reloadData];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Table view data source

- (VLListOfExamsTableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VLExamCell";
    
    VLListOfExamsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)configureCell:(VLListOfExamsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [self setTitleForCell:cell labelText:[NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"Exam", @"Exam"), (int) indexPath.row + 1]];
    
    int percentageDone = 0;
    NSString *subTitle = @"";
    NSString *caption = @"";

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [NSString stringWithFormat:@"VLExamAnswers_%d_%d.plist", APP_EXAM_ANSWERS_FILENAME_VERSION, (int) indexPath.row + 1];
    NSString *documentsPath = [documentsDirectory stringByAppendingPathComponent:file];
    NSDictionary *answers = [[NSDictionary alloc] initWithContentsOfFile:documentsPath];
    
    int numberOfQuestionsAnswered = [(NSString *)[answers valueForKey:@"totaalAantalBeantwoord"] intValue];
    int totalNumberOfQuestions = [(NSString *)[answers valueForKey:@"totaalAantal"] intValue];
    int numberOfQuestionsAnsweredIncorrect = numberOfQuestionsAnswered - [[answers valueForKey:@"totaalAantalGoed"] intValue];
    
    if(numberOfQuestionsAnswered == totalNumberOfQuestions) {
        percentageDone = 100;
    }
    else if(numberOfQuestionsAnswered >= 0) {
        percentageDone = ceil((double)numberOfQuestionsAnswered/totalNumberOfQuestions)*100;
    }
    
    if(numberOfQuestionsAnsweredIncorrect > 1) {
        subTitle = [NSString stringWithFormat:NSLocalizedString(@"ExamResultsPercentageMoreErrors", @"ExamResultsPercentageMoreErrors"), numberOfQuestionsAnswered, numberOfQuestionsAnsweredIncorrect];
    }
    else {
        subTitle = [NSString stringWithFormat:NSLocalizedString(@"ExamResultsPercentageOneError", @"ExamResultsPercentageOneError"), numberOfQuestionsAnswered, numberOfQuestionsAnsweredIncorrect];
    }
    
    [self setSubtitleForCell:cell labelText:subTitle];
    [self setCaptionForCell:cell labelText:caption];
    [self setImageForCell:cell imageName:[NSString stringWithFormat:@"road_a%d", (int)indexPath.row + 1]];
}

- (void)setTitleForCell:(VLListOfExamsTableViewCell *)cell labelText:(NSString *)text {
    [cell.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [cell.titleLabel setNumberOfLines:0];
    [cell.titleLabel setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [cell.titleLabel setText:text];
}

- (void)setSubtitleForCell:(VLListOfExamsTableViewCell *)cell labelText:(NSString *)text {
    [cell.subtitleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.subtitleLabel setNumberOfLines:0];
    [cell.subtitleLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.subtitleLabel setText:text];
}

- (void)setCaptionForCell:(VLListOfExamsTableViewCell *)cell labelText:(NSString *)text {
    [cell.captionLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [cell.captionLabel setNumberOfLines:0];
    [cell.captionLabel setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [cell.captionLabel setText:text];
}

- (void)setImageForCell:(VLListOfExamsTableViewCell *)cell imageName:(NSString *)text {
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@", text]];
    [cell.image setImage:img];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellAtIndexPath:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return APP_NUMBER_OF_EXAMS;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedCellIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    selectedCellRect = [selectedCell bounds];
    
    numberOfExam = (int) indexPath.row + 1;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [NSString stringWithFormat:@"VLExamAnswers_%d_%d.plist", APP_EXAM_ANSWERS_FILENAME_VERSION, (int) indexPath.row + 1];
    documentsPathsForSelectedExam = [documentsDirectory stringByAppendingPathComponent:file];
    answersForSelectedExam = [[NSDictionary alloc] initWithContentsOfFile:documentsPathsForSelectedExam];
    
    numberOfQuestionsAnsweredForSelectedExam = [(NSString *)[answersForSelectedExam valueForKey:@"totaalAantalBeantwoord"] intValue];
    totalNumberOfQuestionsForSelectedExam = [(NSString *)[answersForSelectedExam valueForKey:@"totaalAantal"] intValue];
    numberOfQuestionsAnsweredIncorrectForSelectedExam = numberOfQuestionsAnsweredForSelectedExam - [[answersForSelectedExam valueForKey:@"totaalAantalGoed"] intValue];
    
    if(!isShowingPopover) {
        isShowingPopover = TRUE;
        
        popoverPresentationController =  (VLExamsSelectionViewController *)[[UIStoryboard storyboardWithName:@"Main_Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"VLExamsSelectionViewController"];
        [popoverPresentationController setModalPresentationStyle:UIModalPresentationPopover];
        [popoverPresentationController setPreferredContentSize:CGSizeMake(320,180)];
        
        if(numberOfQuestionsAnsweredIncorrectForSelectedExam > 0) {
            popoverPresentationController.examHasErrors = TRUE;
        }
        else {
            popoverPresentationController.examHasErrors = FALSE;
        }
        
        if(numberOfQuestionsAnsweredForSelectedExam == totalNumberOfQuestionsForSelectedExam) {
            popoverPresentationController.examCompleted = TRUE;
        }
        else {
            popoverPresentationController.examCompleted = FALSE;
        }
        
        popoverController = [popoverPresentationController popoverPresentationController];
        [popoverController setPermittedArrowDirections:UIPopoverArrowDirectionAny];
        [popoverController setBackgroundColor:[UIColor whiteColor]];
        [popoverController setSourceView:selectedCell];
        [popoverController setSourceRect:selectedCellRect];
        [popoverController setDelegate:self];

        [self presentViewController:popoverPresentationController animated:TRUE completion:nil];
    }
}
//
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//}
//
//- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewRowAction *resetAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"ResetSelectedExamButton", @"ResetSelectedExamButton") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
//        numberOfExam = (int) indexPath.row + 1;
//        
//        [self initializeAnswersForSelectedExam];
//    }];
//    
//    [resetAction setBackgroundColor:TABLEVIEW_ROW_RESET_BACKGROUNDCOLOR];
//    
//    return @[resetAction];
//}

# pragma mark - Popover Presentation Controller Delegate

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)controller {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    typeOfExam = [defaults objectForKey:@"typeOfExam"];
    isShowingPopover = FALSE;
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:selectedCellIndexPath];
    [cell setSelected:false];

    [self proceedToQuestionsForExamViewController];
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return YES;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing  _Nonnull *)view {
}

-(UIModalPresentationStyle) adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

-(UIModalPresentationStyle) adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller traitCollection:(UITraitCollection *)traitCollection {
    return UIModalPresentationNone;
}

@end

@implementation VLListOfExamsTableViewController(Private)

- (void)initializeAnswersForSelectedExam {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [NSString stringWithFormat:@"VLExamAnswers_%d_%d.plist", APP_EXAM_ANSWERS_FILENAME_VERSION, numberOfExam];
    documentsPathsForSelectedExam = [documentsDirectory stringByAppendingPathComponent:file];
    answersForSelectedExam = [[NSDictionary alloc] initWithContentsOfFile:documentsPathsForSelectedExam];
    
    numberOfQuestionsAnsweredForSelectedExam = [(NSString *)[answersForSelectedExam valueForKey:@"totaalAantalBeantwoord"] intValue];
    totalNumberOfQuestionsForSelectedExam = [(NSString *)[answersForSelectedExam valueForKey:@"totaalAantal"] intValue];
    numberOfQuestionsAnsweredIncorrectForSelectedExam = numberOfQuestionsAnsweredForSelectedExam - [[answersForSelectedExam valueForKey:@"totaalAantalGoed"] intValue];

    if(numberOfQuestionsAnsweredForSelectedExam > 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ResetSelectedExamTitle", @"ResetSelectedExamTitle") message:NSLocalizedString(@"ResetSelectedExamMessage", @"ResetSelectedExamMessage") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Yes", @"Yes")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [answersForSelectedExam setValue:@"0" forKey:@"totaalAantalBeantwoord"];
                                        [answersForSelectedExam setValue:@"0" forKey:@"totaalAantalGoed"];
                                       
                                        int max = [[answersForSelectedExam valueForKey:@"totaalAantal"] intValue];
                                    
                                        for (int i = 0; i < max; i++) {
                                            [[[answersForSelectedExam valueForKey:@"antwoorden"] objectAtIndex:i] setValue:@"NA" forKey:@"antwoordWasGoed"];
                                        }
                                       
                                        [answersForSelectedExam writeToFile:documentsPathsForSelectedExam atomically:YES];
                                    
                                        [self.tableView reloadData];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"No", @"No")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self.tableView reloadData];
                                   }];
    
        [alert addAction:yesButton];
        [alert addAction:noButton];
    
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        [self.tableView reloadData]; 
    }
}

- (void)proceedToQuestionsForExamViewController {
    if (![typeOfExam isEqualToString:@"None"] && !isShowingNextViewController){
        isShowingNextViewController = TRUE;
            
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_Storyboard" bundle: nil];
        VLQuestionsForExamViewController *questionsViewController = (VLQuestionsForExamViewController*)[storyboard instantiateViewControllerWithIdentifier: @"VLQuestionsForExamViewController"];
            
        questionsViewController.typeOfExam = typeOfExam;
        questionsViewController.numberOfExam = numberOfExam;
            
        [self.navigationController pushViewController:questionsViewController animated:YES];
    }
}

@end



