//
//  VLListOfTrafficSignsPageContentViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 05/01/15.
//  Copyright (c) 2015 Bart Bruijnesteijn. All rights reserved.
//

#import "VLListOfTrafficSignsPageContentViewController.h"
#import "VLConstants.h"

@interface VLListOfTrafficSignsPageContentViewController ()

- (void)initializeDataSource;

@end

@implementation VLListOfTrafficSignsPageContentViewController

@synthesize currentPageNumber, numberOfPages, signImage, signImageName, sign, signLabelText, category, categoryLabelText, page, pageLabelText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initializeDataSource];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self initializeDataSource];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Table view delegate

- (void)initializeDataSource {
    [self.category setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [self.category setMinimumScaleFactor:0.5];
    [self.category setTextAlignment:NSTextAlignmentCenter];
    [self.category setLineBreakMode: NSLineBreakByWordWrapping];
    [self.category setNumberOfLines:0];
    [self.category setTextColor:TABLEVIEW_ROW_TITLE_COLOR];
    [self.category setText:self.categoryLabelText];
    
    [self.sign setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
    [self.sign setMinimumScaleFactor:0.5];
    [self.sign setTextAlignment:NSTextAlignmentCenter];
    [self.sign setLineBreakMode:NSLineBreakByTruncatingTail];
    [self.sign setTextColor:TABLEVIEW_ROW_SUBTITLE_COLOR];
    [self.sign setText:self.signLabelText];
    
    [self.page.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.page.titleLabel setMinimumScaleFactor:0.5];
    [self.page.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.page.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    [self.page.titleLabel setTextColor:[UIColor whiteColor]];
    [self.page setTitle:[NSString stringWithFormat:@"%lu - %lu", (unsigned long)self.currentPageNumber + 1, (unsigned long)self.numberOfPages] forState:UIControlStateNormal];
    
    NSString *file = [NSString stringWithFormat:@"%@/%@%@.png", [[NSBundle mainBundle] resourcePath], self.signImageName, @"@3x"];
    [UIImage imageWithContentsOfFile:file];
    [self.signImage setImage:[UIImage imageNamed:self.signImageName]];
}

@end
