//
//  VLSelectCriteriaForTrafficSignsTableViewController.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/19/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import "VLSelectCriteriaForTrafficSignsTableViewController.h"
#import "VLListOfTrafficSignsViewController.h"
#import "VLConstants.h"
#import "VLLocalizationUtility.h"

@interface VLSelectCriteriaForTrafficSignsTableViewController(Private)

- (void)initializeTextStyle;
- (IBAction)buttonTapped:(id)sender;
- (void)removeFromSearchCriteria:(NSString *) criteria;
- (void)findMatchingTrafficSignsForSearchCriteria;

@end

@implementation VLSelectCriteriaForTrafficSignsTableViewController

@synthesize dataSource, circleImageView, squareImageView, diamondImageView, rectangleLandscapeImageView ,rectanglePortraitImageView ,triangleBottomImageView ,triangleTopImageView, blueButton, redButton, yellowButton, greenButton, blackButton, numberButton, letterButton, textButton, carImageView, truckImageView, motorImageView, mopedImageView, bicycleImageView, personImageView, busImageView, trainImageView, tractorImageView, animalImageView, arrowImageView, otherButton, searchCriteria, signsMatchingCriteria;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    searchCriteria = [[NSMutableArray alloc] init];
    signsMatchingCriteria = [[NSMutableDictionary alloc] init];

    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"NextSmall"] style:UIBarButtonItemStylePlain target:self action:@selector(findMatchingTrafficSignsForSearchCriteria)];
    [rightButton setTintColor:APP_NAVIGATION_BAR_TINT];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChangedNotification:)
                                                 name:@"LanguageChangedNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficSignSelectionTitle", @"TrafficSignSelectionTitle")];
        
    [self.tableView reloadData];

    UITapGestureRecognizer *circleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedShape:)];
    circleTap.numberOfTapsRequired = 1;
    [circleImageView addGestureRecognizer:circleTap];

    UITapGestureRecognizer *squareTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedShape:)];
    squareTap.numberOfTapsRequired = 1;
    [squareImageView addGestureRecognizer:squareTap];
    
    UITapGestureRecognizer *diamondTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedShape:)];
    diamondTap.numberOfTapsRequired = 1;
    [diamondImageView addGestureRecognizer:diamondTap];
    
    UITapGestureRecognizer *rectangleLandscapeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedShape:)];
    rectangleLandscapeTap.numberOfTapsRequired = 1;
    [rectangleLandscapeImageView addGestureRecognizer:rectangleLandscapeTap];
    
    UITapGestureRecognizer *rectanglePortraitTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedShape:)];
    rectanglePortraitTap.numberOfTapsRequired = 1;
    [rectanglePortraitImageView addGestureRecognizer:rectanglePortraitTap];
    
    UITapGestureRecognizer *triangleTopTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedShape:)];
    triangleTopTap.numberOfTapsRequired = 1;
    [triangleTopImageView addGestureRecognizer:triangleTopTap];
    
    UITapGestureRecognizer *triangleBottomTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedShape:)];
    triangleBottomTap.numberOfTapsRequired = 1;
    [triangleBottomImageView addGestureRecognizer:triangleBottomTap];
    
    UITapGestureRecognizer *carTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    carTap.numberOfTapsRequired = 1;
    [carImageView addGestureRecognizer:carTap];
    
    UITapGestureRecognizer *truckTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    truckTap.numberOfTapsRequired = 1;
    [truckImageView addGestureRecognizer:truckTap];
    
    UITapGestureRecognizer *motorTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    motorTap.numberOfTapsRequired = 1;
    [motorImageView addGestureRecognizer:motorTap];
    
    UITapGestureRecognizer *mopedTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    mopedTap.numberOfTapsRequired = 1;
    [mopedImageView addGestureRecognizer:mopedTap];
    
    UITapGestureRecognizer *bicycleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    bicycleTap.numberOfTapsRequired = 1;
    [bicycleImageView addGestureRecognizer:bicycleTap];
    
    UITapGestureRecognizer *personTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    personTap.numberOfTapsRequired = 1;
    [personImageView addGestureRecognizer:personTap];
    
    UITapGestureRecognizer *busTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    busTap.numberOfTapsRequired = 1;
    [busImageView addGestureRecognizer:busTap];
    
    UITapGestureRecognizer *trainTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    trainTap.numberOfTapsRequired = 1;
    [trainImageView addGestureRecognizer:trainTap];
    
    UITapGestureRecognizer *tractorTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    tractorTap.numberOfTapsRequired = 1;
    [tractorImageView addGestureRecognizer:tractorTap];
    
    UITapGestureRecognizer *animalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    animalTap.numberOfTapsRequired = 1;
    [animalImageView addGestureRecognizer:animalTap];
    
    UITapGestureRecognizer *arrowTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
    arrowTap.numberOfTapsRequired = 1;
    [arrowImageView addGestureRecognizer:arrowTap];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)languageChangedNotification:(NSNotification *) notification
{
    [self.tableView reloadData];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];

    [[self navigationItem] setTitle:NSLocalizedString(@"TrafficSignSelectionTitle", @"TrafficSignSelectionTitle")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [self releaseIBOutlets];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle;
    
    switch (section)
    {
        case 0:
            sectionTitle = NSLocalizedString(@"TrafficSignShapeSelectionTitle", @"TrafficSignShapeSelectionTitle");
            
            break;
            
        case 1:
            sectionTitle = NSLocalizedString(@"TrafficSignColorSelectionTitle", @"TrafficSignColorSelectionTitle");
            
            break;
            
        case 2:
            sectionTitle = NSLocalizedString(@"TrafficSignTextSelectionTitle", @"TrafficSignTextSelectionTitle");
            
            break;
            
        case 3:
            sectionTitle = NSLocalizedString(@"TrafficSignImageSelectionTitle", @"TrafficSignImageSelectionTitle");
            
            break;
            
        default:
            sectionTitle = @"";
            
            break;
    }
    
    return sectionTitle;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *) view;
    [header.contentView setBackgroundColor:TABLEVIEW_SECTION_COLOR];
    [header.contentView setAlpha:0.6];
    [header.textLabel setTextColor:TABLEVIEW_SECTION_TEXTCOLOR];
    [header.textLabel setOpaque:NO];
    [header.textLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tappedShape:(id)sender
{
    [self.circleImageView setAlpha:1.0];
    [self removeFromSearchCriteria:@"CIRCLE"];
    [self.squareImageView setAlpha:1.0];
    [self removeFromSearchCriteria:@"SQUARE"];
    [self.diamondImageView setAlpha:1.0];
    [self removeFromSearchCriteria:@"DIAMOND"];
    [self.rectangleLandscapeImageView setAlpha:1.0];
    [self removeFromSearchCriteria:@"RECTANGLE_LANDSCAPE"];
    [self.rectanglePortraitImageView setAlpha:1.0];
    [self removeFromSearchCriteria:@"RECTANGLE_PORTRAIT"];
    [self.triangleBottomImageView setAlpha:1.0];
    [self removeFromSearchCriteria:@"TRIANGLE_BOTTOM"];
    [self.triangleTopImageView setAlpha:1.0];
    [self removeFromSearchCriteria:@"TRIANGLE_TOP"];
    [self removeFromSearchCriteria:@"OTHER_SHAPE"];

    UIImageView *view = (UIImageView *)[sender view];
    int selectedView = (int) [view tag];
    
    if ([view alpha] == 1.0) {
        [view setAlpha:0.3];
        
        switch (selectedView) {
            case 1:
                [searchCriteria addObject:@"CIRCLE"];
                
                break;
                
            case 2:
                [searchCriteria addObject:@"SQUARE"];
                
                break;
                
            case 3:
                [searchCriteria addObject:@"DIAMOND"];
                
                break;
                
            case 4:
                [searchCriteria addObject:@"RECTANGLE_LANDSCAPE"];
                
                break;
                
            case 5:
                [searchCriteria addObject:@"RECTANGLE_PORTRAIT"];
                
                break;
                
            case 6:
                [searchCriteria addObject:@"TRIANGLE_BOTTOM"];
                
                break;
                
            case 7:
                [searchCriteria addObject:@"TRIANGLE_TOP"];
                
                break;
                
            default:
                [searchCriteria addObject:@"OTHER_SHAPE"];

                break;
                
        }
    }
    else {
        [view setAlpha:1.0];
    }
}

- (void)tappedImage:(id)sender
{
    UIImageView *view = (UIImageView *)[sender view];
    int selectedView = (int) [view tag];

    if ([view alpha] == 1.0) {
        [view setAlpha:0.3];
        
        switch (selectedView) {
            case 16:
                [searchCriteria addObject:@"CAR"];
                
                break;
                
            case 17:
                [searchCriteria addObject:@"TRUCK"];
                
                break;
                
            case 18:
                [searchCriteria addObject:@"MOTOR"];
                
                break;
                
            case 19:
                [searchCriteria addObject:@"MOPED"];
                
                break;
                
            case 20:
                [searchCriteria addObject:@"BICYCLE"];
                
                break;
                
            case 21:
                [searchCriteria addObject:@"PERSON"];
                
                break;
                
            case 22:
                [searchCriteria addObject:@"BUS"];
                
                break;
                
            case 23:
                [searchCriteria addObject:@"TRAIN"];
                
                break;
                
            case 24:
                [searchCriteria addObject:@"TRACTOR"];
                
                break;
                
            case 25:
                [searchCriteria addObject:@"ANIMAL"];
                
                break;
                
            case 26:
                [searchCriteria addObject:@"ARROW"];
                
                break;
               
            default:
                break;
                
        }
    }
    else {
        [view setAlpha:1.0];
        
        switch (selectedView) {
            case 16:
                [self removeFromSearchCriteria:@"CAR"];
                
                break;
                
            case 17:
                [self removeFromSearchCriteria:@"TRUCK"];
                
                break;
                
            case 18:
                [self removeFromSearchCriteria:@"MOTOR"];
                
                break;
                
            case 19:
                [self removeFromSearchCriteria:@"MOPED"];
                
                break;
                
            case 20:
                [self removeFromSearchCriteria:@"BICYCLE"];
                
                break;
                
            case 21:
                [self removeFromSearchCriteria:@"PERSON"];
                
                break;
                
            case 22:
                [self removeFromSearchCriteria:@"BUS"];
                
                break;
                
            case 23:
                [self removeFromSearchCriteria:@"TRAIN"];
                
                break;
                
            case 24:
                [self removeFromSearchCriteria:@"TRACTOR"];
                
                break;
                
            case 25:
                [self removeFromSearchCriteria:@"ANIMAL"];
                
                break;
                
            case 26:
                [self removeFromSearchCriteria:@"ARROW"];
                
                break;
                
            default:
                break;
                
        }
    }
}

- (void)releaseIBOutlets {
    self.circleImageView = nil;
    self.squareImageView = nil;
    self.diamondImageView = nil;
    self.rectangleLandscapeImageView = nil;
    self.rectanglePortraitImageView = nil;
    self.triangleBottomImageView = nil;
    self.triangleTopImageView = nil;
    
    self.blueButton = nil;
    self.redButton = nil;
    self.yellowButton = nil;
    self.greenButton = nil;
    self.blackButton = nil;
    
    self.numberButton = nil;
    self.letterButton = nil;
    self.textButton = nil;
    
    self.carImageView = nil;
    self.truckImageView = nil;
    self.motorImageView = nil;
    self.mopedImageView = nil;
    self.bicycleImageView = nil;
    self.personImageView = nil;
    self.busImageView = nil;
    self.trainImageView = nil;
    self.tractorImageView = nil;
    self.animalImageView = nil;
    self.arrowImageView = nil;
    self.otherButton = nil;
}

@end

@implementation VLSelectCriteriaForTrafficSignsTableViewController(Private)

- (void)initializeTextStyle {
    [self.numberButton.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.letterButton.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.textButton.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
    [self.otherButton.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];
}

- (IBAction)buttonTapped:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (button.layer.borderWidth == 0.0) {
        [button.layer setBorderWidth:3.0];
        
        switch ([sender tag]) {
            case 8:
                [button.layer setBorderColor:SIGN_SELECTION_BLUE_COLOR.CGColor];
                [searchCriteria addObject:@"BLUE"];
                
                break;

            case 9:
                [button.layer setBorderColor:SIGN_SELECTION_RED_COLOR.CGColor];
                [searchCriteria addObject:@"RED"];
                
                break;
                
            case 10:
                [button.layer setBorderColor:SIGN_SELECTION_YELLOW_COLOR.CGColor];
                [searchCriteria addObject:@"YELLOW"];
                
                break;
                
            case 11:
                [button.layer setBorderColor:SIGN_SELECTION_GREEN_COLOR.CGColor];
                [searchCriteria addObject:@"GREEN"];
                
                break;
                
            case 12:
                [button.layer setBorderColor:SIGN_SELECTION_LIGHTGRAY_COLOR.CGColor];
                [searchCriteria addObject:@"BLACK"];
                
                break;
                
            case 13:
                [button.layer setBorderColor:SIGN_SELECTION_GRAY_COLOR.CGColor];
                [searchCriteria addObject:@"NUMBERS"];
                
                break;
                
            case 14:
                [button.layer setBorderColor:SIGN_SELECTION_GRAY_COLOR.CGColor];
                [searchCriteria addObject:@"LETTERS"];
                
                break;
                
            case 15:
                [button.layer setBorderColor:SIGN_SELECTION_GRAY_COLOR.CGColor];
                [searchCriteria addObject:@"WORDS"];
                
                break;
                
            case 27:
                [button.layer setBorderColor:SIGN_SELECTION_GRAY_COLOR.CGColor];
                [searchCriteria addObject:@"OTHER_IMAGE"];
                
                break;
                
            default:
                [button.layer setBorderColor:SIGN_SELECTION_GRAY_COLOR.CGColor];
                
                break;
                
        }
    }
    else {
        [button.layer setBorderWidth:0.0];
        [button.layer setBorderColor:[UIColor whiteColor].CGColor];
        
        switch ([sender tag]) {
            case 8:
                [self removeFromSearchCriteria:@"BLUE"];
                
                break;
                
            case 9:
                 [self removeFromSearchCriteria:@"RED"];
                
                break;
                
            case 10:
                  [self removeFromSearchCriteria:@"YELLOW"];
                
                break;
                
            case 11:
                   [self removeFromSearchCriteria:@"GREEN"];
                
                break;
                
            case 12:
                [self removeFromSearchCriteria:@"BLACK"];
                
                break;
                
            case 13:
                [self removeFromSearchCriteria:@"NUMBERS"];
                
                break;
                
            case 14:
                [self removeFromSearchCriteria:@"LETTERS"];
                
                break;
                
            case 15:
                [self removeFromSearchCriteria:@"WORDS"];
                
                break;
                
            case 27:
                [self removeFromSearchCriteria:@"OTHER_IMAGE"];
                
                break;
                
            default:
                break;
                
        }
    }
}

- (void)removeFromSearchCriteria:(NSString *) criteria {
    int count = (int) [searchCriteria count];
    
    for (int i = 0; i < count; i++) {
        if ([[searchCriteria objectAtIndex:i] isEqualToString:criteria]) {
            [searchCriteria removeObjectAtIndex:i];
            count--;
        }
    }
}

- (void)findMatchingTrafficSignsForSearchCriteria {
    NSMutableArray *matchingTrafficSigns = [[NSMutableArray alloc] init];
    
    [signsMatchingCriteria setObject:NSLocalizedString(@"Traffic signs matching search criteria", @"Traffic signs matching search criteria") forKey:@"beschrijving"];
    [signsMatchingCriteria setObject:NSLocalizedString(@"Matching Traffic Signs", @"Matching Traffic Signs") forKey:@"categorie"];
    [signsMatchingCriteria setObject:@"Q" forKey:@"id"];
    
    if([searchCriteria count] > 0) {
        NSString *path = [[NSBundle mainBundle] bundlePath];
        NSString *plistPath = nil;
        
        plistPath = [path stringByAppendingPathComponent:NSLocalizedString(@"VLTrafficSignRVV_datasource", @"VLTrafficSignRVV_datasource")];

        self.dataSource = [NSArray arrayWithContentsOfFile:plistPath];
        int numberOfCategories = (int)[dataSource count];
        
        for(int i = 0; i < numberOfCategories; i++) {
            NSString *categoryNameForCurrentCategory = [[self.dataSource objectAtIndex:i] objectForKey:@"categorie"];
            NSArray *signsInCurrentCategory = [[self.dataSource objectAtIndex:i] objectForKey:@"verkeersborden"];
            int numberSignsInSelectedCategory = (int)[signsInCurrentCategory count];

            for(int j = 0; j < numberSignsInSelectedCategory; j++) {
                BOOL matchFound = FALSE;
                
                NSMutableDictionary *currentSignInCurrentCategory = [[signsInCurrentCategory objectAtIndex:j] mutableCopy];
                NSString *criteria = [currentSignInCurrentCategory objectForKey:@"criteria"];
                
                int numberOfCriteria = (int)[searchCriteria count];
                do {
                    numberOfCriteria--;
                    matchFound = [criteria containsString:[searchCriteria objectAtIndex:numberOfCriteria]];
                } while (matchFound && numberOfCriteria > 0);
                    
                if(matchFound) {
                    [currentSignInCurrentCategory setValue:categoryNameForCurrentCategory forKey:@"categorie"];
                    [matchingTrafficSigns addObject:currentSignInCurrentCategory];
                }
            }
        }
    }

    [signsMatchingCriteria setObject:matchingTrafficSigns forKey:@"verkeersborden"];
    
    if ([matchingTrafficSigns count] > 0){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_Storyboard" bundle: nil];
        VLListOfTrafficSignsViewController *trafficSignsViewController = (VLListOfTrafficSignsViewController*)[storyboard instantiateViewControllerWithIdentifier: @"VLListOfTrafficSignsViewController"];
        
        trafficSignsViewController.selectedTrafficSigns = [[NSMutableArray alloc] init];
        [trafficSignsViewController.selectedTrafficSigns addObject:signsMatchingCriteria];
        
        [self.navigationController pushViewController:trafficSignsViewController animated:YES];
    }
    else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NoTrafficSignsFoundTitle", @"NoTrafficSignsFoundTitle") message:NSLocalizedString(@"NoTrafficSignsFoundMessage", @"NoTrafficSignsFoundMessage") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Ok", @"Ok")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self.tableView reloadData];
                                   }];
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
