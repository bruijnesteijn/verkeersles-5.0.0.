//
//  VLTheoryCategory.m
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 7/9/11.
//  Copyright (c) 2011 2010 PiecesOfMyMind Software. All rights reserved.
//

#import "VLTheoryCategory.h"
#import "VLTheoryItem.h"

@implementation VLTheoryCategory

@dynamic theoryCategoryDescription;
@dynamic theoryCategoryIcon;
@dynamic theoryCategoryBeschrijving;
@dynamic theoryCategoryNaam;
@dynamic theoryCategoryID;
@dynamic theoryCategoryName;

@dynamic hasVLTheoryItem;

- (void)addHasVLTheoryItemObject:(VLTheoryItem *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"hasVLTheoryItem"] addObject:value];
    [self didChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
}

- (void)removeHasVLTheoryItemObject:(VLTheoryItem *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"hasVLTheoryItem"] removeObject:value];
    [self didChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
}

- (void)addHasVLTheoryItem:(NSSet *)value {
    [self willChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"hasVLTheoryItem"] unionSet:value];
    [self didChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeHasVLTheoryItem:(NSSet *)value {
    [self willChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"hasVLTheoryItem"] minusSet:value];
    [self didChangeValueForKey:@"hasVLTheoryItem" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}

@end
