//
//  VLListOfTrafficSignsTableViewCell.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 5/5/10.
//  Copyright 2010 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLListOfTrafficSignsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *image;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;

@end
