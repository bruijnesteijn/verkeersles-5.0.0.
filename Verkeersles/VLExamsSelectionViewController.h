//
//  VLExamsSelectionViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 11/11/15.
//  Copyright © 2015 Bart Bruijnesteijn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLExamsSelectionViewController : UIViewController {
    BOOL examHasErrors;
    BOOL examCompleted;
    
    IBOutlet UIImageView *firstImage;
    IBOutlet UILabel *firstTitleLabel;
    IBOutlet UIImageView *secondImage;
    IBOutlet UILabel *secondTitleLabel;
    IBOutlet UIImageView *thirdImage;
    IBOutlet UILabel *thirdTitleLabel;
}

@property BOOL examHasErrors;
@property BOOL examCompleted;

@property (nonatomic, retain) IBOutlet UIImageView *firstImage;
@property (nonatomic, retain) IBOutlet UILabel *firstTitleLabel;
@property (nonatomic, retain) IBOutlet UIImageView *secondImage;
@property (nonatomic, retain) IBOutlet UILabel *secondTitleLabel;
@property (nonatomic, retain) IBOutlet UIImageView *thirdImage;
@property (nonatomic, retain) IBOutlet UILabel *thirdTitleLabel;

@property (weak, nonatomic) IBOutlet UIStackView *firstStackView;
@property (weak, nonatomic) IBOutlet UIStackView *secondStackView;
@property (weak, nonatomic) IBOutlet UIStackView *thirdStackView;

@end
