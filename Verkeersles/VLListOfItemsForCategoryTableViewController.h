//
//  VLListOfItemsForCategoryTableViewController.h
//  Verkeersles
//
//  Created by Bart Bruijnesteijn on 2/19/11.
//  Copyright 2011 PiecesOfMyMind Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLTheoryCategory.h"

@interface VLListOfItemsForCategoryTableViewController : UITableViewController
<NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate> {
    VLTheoryCategory *category;
    
    NSString *searchOneCategoryPredicate;
    NSString *searchAllCategoriesPredicate;
    BOOL searchAllCategories;
    
    NSInteger index;

@private
    NSFetchedResultsController *_fetchedResultsController;
    NSUInteger sectionInsertCount;
}

@property (nonatomic, retain) NSManagedObject *category;

@property (nonatomic, retain) NSString *searchOneCategoryPredicate;
@property (nonatomic, retain) NSString *searchAllCategoriesPredicate;
@property (nonatomic) BOOL searchAllCategories;

@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) UISearchController *searchOneController;

@end
